---
layout: page
title: Shop
subtitle: Nearly new and second-hand stuff.
permalink: /shop/
---

I sell stuff that I don't need anymore.
Standard 15% discount when you pay in Monero (XMR), except on shipping or where otherwise noted.

Shipping is via [bpost](http://www.bpost.be/):

**National shipping (Belgium):**

| Weight  | Price |
|---------|-------|
| 0 - 2kg |  €5,- |
| 2 -10kg |  €6,- |

Insurance is €1.

**EU shipping:**

| Weight     | Price | Price with tracking |
|------------|-------|---------------------|
| 0 - 350g   | €8,75 | €16,40              |
| 350g - 1kg | €13,05| €16,40              |
| 1 - 2kg    | €17,40| €16,40              |

Insurance is available too, but those tables become very long.
Feel free to contact me on *shop[at]rubdos[dot]be*

---


## ThinkPads

I have a bunch of vintage Thinkpads for sale.
They have all been tested with a live image of Ubuntu 16.04.3 LTS.
Also keep in mind that I am a Lenovo business partner;
I may or may not be able to get my hands on replacement parts.

![The stash ThinkPads](https://i.imgur.com/3d3ciP7.jpg)

Note for non-technical people:
feel free to contact me if you think "I need a simple computer for cheap": *shop[at]rubdos[dot]be*

Pics on request, please mention the serial number when requesting pics.

**Thinkpads come per default without a battery or charger**,
since I have less chargers and batteries (3) than I have computers.
Chargers and batteries (0.5kg) are €20,-.
Best two batteries are at 80%, third battery is at 58% capacity.

*HDD's, when present, come wiped and zeroed out*

I also have (Type 2504) docks that are compatible with the T60p and the T61p.
They come at 30€/pc.

For an additional €20, I will install a standard Linux distribution.
Feel free to ask me to put aftermarket upgrades in it; SSD's or extra RAM will greatly benefit those computers.

### IBM Thinkpad T60p (2008-8HG)

Asking price: **€110**
Weight: 2kg

I currently have ~~five~~ four Thinkpad T60p's for sale.
They are equally specced, but not all are in the same condition; make your pick.

Cool features:
- Real docking connector
- ThinkVantage button
- 7 row keyboard
- Two finger TrackPad
- DVD recorder!
- Physical WiFi kill switch

Specs:
- Intel Core2Duo T7600 @ 2.33GHz (64-bit)
- 2GB RAM
- SXGA+ 1400x1050 4:3 screen
- 100GB HDD
- Gigabit Ethernet
- Intel PRO 3145ABG WLAN
- AMD/ATI FireGL V5250
- Belgian AZERTY keyboard

Those ~~five~~ four are the ones I have on sale:

- *L3-HV127*: minor plastic crack at the fan. I'll superglue it for you.
- *L3-HV131*: the screen backlight has a reddish colour the first few seconds;
            after it boots, it's alright. WiFi kill switch cracked, but still works
- *L3-RP260*: no remarks
- ~~*L3-HZ331*: QWERTY keyboard~~ SOLD
- *L3-HZ332*: QWERTY keyboard

### IBM Thinkpad T61p (6458-5KG)

Asking price: **€160**
Weight: 2,5kg

Cool features:
- Real docking connector
- ThinkVantage button
- 7 row keyboard
- Two finger TrackPad
- DVD recorder!
- Better-than-FullHD screen!
- Physical WiFi kill switch
- Belgian AZERTY keyboard
- Spill proof keyboard

Specs:
- Intel Core2Duo T7700 @ 2.4GHz (64-bit)
- 4GB RAM
- WUXGA 1920 x 1200 screen
- 100GB HDD
- Gigabit Ethernet
- Intel PRO 4965ABG WLAN
- NVidia Quadro FX 570M

Those two are the ones I have on sale:

- *L3-M3493*: no remarks
- *L3-M3487*: no remarks

## Miscellaneous

Other stuff that I want to get rid of.
Most of it nearly new, packaging opened, but (almost) unused.

### TV cards

I got two TeVii S482 DVB-S2 dual tuner cards, in good (nearly new) condition.
They don't work when you put two of them in one system, for some odd reason.
They do work *very well* (as in: mainline Linux support) separately though.

Price: **€60** per piece.

### SATA controllers

[DeLOCK Dual port Serial-ATA 3G/s PCIe x1 Controller](https://www.alternate.be/DeLOCK/Dual-poort-Serial-ATA-3G-s-PCIe-adapter-Controller/html/product/664758?tk=7&lk=8969)
Verified working perfectly under Windows, but weird under Linux.
**€19**

[Digitus SATAIII PCIe x2 4xSATA 2xeSATA Controller](https://www.alternate.be/html/product/1159528)
Works only if you can actually physically attach a monitor to the system,
since even in non-RAID mode, you need to assign the disks to be JBOD.
So if you don't have embedded graphics, you *require a GPU and a free x2 slot*.
After configuration, no monitor is needed anymore.
**€55**
