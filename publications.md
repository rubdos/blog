---
layout: page
title: Academia
subtitle: My academic publications
permalink: /academic/

msc:
- name: Van de Velde, S.
  year: 2023
  title: "Zero-knowledge proofs for anonymous authentication mechanisms"
  location: Vrije Universiteit Brussel. Belgium
- name: Peeters, D.
  year: 2023
  title: "Realtime Energy Market Datastreams from a Prosumer-Perspective: towards a prosumer-driven smart grid"
  location: Vrije Universiteit Brussel. Belgium
- name: Blancquaert, R.
  year: 2022
  title: "Elliptic curve field arithmetic speed-up using SIMD instructions"
  location: Vrije Universiteit Brussel. Belgium
- name: Ma, Y.
  year: 2022
  title: "Investigation of Electrical Vehicle Charging Protocols"
  location: Vrije Universiteit Brussel. Belgium
- name: Michalopoulos, A.
  year: 2022
  title: "Enhancing V2G (Vehicle-to-grid) communication protocols"
  location: Vrije Universiteit Brussel. Belgium
- name: Vercammen, J. E.
  year: 2022
  title: "Enabling privacy preserving derived predicates in W3C Verifiable Credentials"
  location: Vrije Universiteit Brussel. Belgium
- name: Akande, M. T.
  year: 2022
  title: "Investigation of electrical vehicle charging protocols Implementation and evaluation of a proxy server based on the Open Charge Point Protocol (OCPP)"
  location: Vrije Universiteit Brussel. Belgium
- name: Godden, T.
  year: 2020
  title: "Blindly enforcing access control policies on encrypted data using zero-knowledge: access control for privacy-focussed, peer-to-peer online social media applications"
  location: Vrije Universiteit Brussel. Belgium
- name: Assi, G.
  year: 2020
  title: "Efficient control of secured wireless sensor and actuator networks using a Keccak sponge: an implementation for a competitive chess environment"
  location: Vrije Universiteit Brussel. Belgium
- name: El Khattuti, Y.
  year: 2020
  title: "Implementation and evaluation of a Keccak-based symmetric authentication Protocol on RFID"
  location: Vrije Universiteit Brussel. Belgium
- name: Contryn, A.
  year: 2019
  title: "Secure communication between IoT devices for short messages"
  location: Vrije Universiteit Brussel. Belgium

---

I'm a teaching assistant and PhD candidate at the [Vrije Universiteit Brussel](https://vub.ac.be/),
at the [ETRO](http://www.etrovub.be/)-[IRIS](http://www.etrovub.be/RESEARCH/IRIS/SmartNets/) lab.

My research interests are cryptography and privacy engineering.
In particular, I research tools that support building peer-to-peer online social networks.

<hr />
<h2>Publications</h2>

{% assign publications = site.publications | sort: 'date' | reverse %}
<ul style="list-style: none;">
{% for publication in publications %}
  <li itemscope itemtype="http://schema.org/ScholarlyArticle" itemid="https://dx.doi.org/{{ publication.doi }}">
    <h3>
      <a href="https://dx.doi.org/{{ publication.doi }}">
        <time itemprop="sdDatePublished" style="font-weight: bold;">{{ publication.date | date: '%Y' }}</time>:
        <span itemprop="headline" style="font-style: italic;">{{ publication.title }}.</span>
      </a>
    </h3>
    ({% for author in publication.authors %}{% if forloop.last %}, and {% elsif forloop.first %}{% else %}, {% endif %}<author itemprop="author">{{ author }}</author>{% endfor %})
    <br />
    {% if publication.doi %}
      {{ publication.doi_text }}
      <a href="https://dx.doi.org/{{ publication.doi }}" itemprop="sameAs">
        https://dx.doi.org/{{ publication.doi }}
      </a>
      {% if publication.preprint || publication.full_text %}
        or
      {% endif %}
    {% endif %}
    {% if publication.preprint %}
      download the <a href="{{ publication.preprint }}" itemprop="url">PDF of the pre-print</a>
    {% endif %}
    {% if publication.full_text %}
      download the <a href="{{ publication.full_text }}" itemprop="url">full text</a>
    {% endif %}

    <br />
    <br />
    <span style="font-weight: bold;">abstract</span>
    <div itemprop="description">
    {{ publication.content }}
    </div>
    <hr/>
  </li>
{% endfor %}
</ul>

<hr />
<h2>Master Thesis supervision</h2>

<ul>
  {% for thesis in page.msc %}
  <li>
    <strong>{{ thesis.name }}</strong> ({{ thesis.year }}).
    <em>
      {{ thesis.title }}
    </em> (Master Thesis). {{ thesis.location }}.
  </li>
  {% endfor %}
</ul>

<hr />

## Project management and contribution

### [RustIEC](https://en.rustiec.be/team.html)

Currently participating in the RustIEC VLAIO TETRA project (VLAIO TETRA HBC.2021.0066).
The goal of the project is to teach Flanders’ companies to be proficient in the Rust programming language.
More information about the project can be found on [the RustIEC project website](https://www.rustiec.be).
