require 'zip'
require 'yaml'

class String
  def url_safe
    self.gsub(" ", "-").downcase
  end
end

module Jekyll
  class MusicPage < Page
    ATTRIBUTES_FOR_LIQUID = %w[
      content
      title
			artist
			song
      url
			midiname
			pdfname
    ]

    attr_reader :artist, :song, :title, :midiname, :pdfname

    def initialize(site, base, dir, entry)
      @site = site
      @base = base
      @dir = dir

      contents = YAML.load(entry.get_input_stream.read)

      @song = contents['name']
      @artist = contents['artist']
      @title = @song + ' by ' + @artist
      basename = entry.name[0..-5]
      @midiname = basename + '.midi'
      @pdfname = basename + '.pdf'

      self.generate_url contents

      self.read_yaml(File.join(base, '_includes', 'music'), 'sheetmusic.md')
    end

    def generate_url(song)
			@dir = File.join(@dir, @artist.url_safe)

			@name = @song.url_safe

      self.process(@name)
			@url = File.join(@dir, @name + '.html')
    end
  end

  class MusicPageGenerator < Generator
    def generate(site)
      repo = site.config['music-repo']
			Zip::File.open(repo) do |input|
				input.each { |entry|
					if entry.file? and entry.name.end_with? '.yml'
						if entry.name.end_with? 'artist.yml'
							# add artist
						else
                            begin
                              page = MusicPage.new(site, site.source, 'music', entry)
                              site.collections["music"].docs << page
                            rescue
                            end
						end
					end
				}
			end
    end
  end
end

