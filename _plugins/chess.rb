require 'archive/tar/minitar'
require 'open-uri'
require 'pgn'

class String
  def url_safe
    self.gsub(" ", "-").downcase
  end
end

module Jekyll
  class ChessGamePage < Page
    ATTRIBUTES_FOR_LIQUID = %w[
      content
      title
      game_title
      game_subtitle
      url
      date
      result
      game
    ]

    attr_reader :game_title, :game_subtitle, :title, :date, :result, :game

    def initialize(site, base, dir, entry)
      @site = site
      @base = base
      @dir = dir

      game = PGN.parse(entry.read).first
      white = game.tags["White"]
      black = game.tags["Black"]
      raise "Missing names in " + entry.full_name if white.empty? or black.empty? or white == "Unknown" or black == "Unknown"

      @date = Date.parse(game.tags["Date"])
      @result = game.result

      self.generate_url game

      self.read_yaml(File.join(base, '_includes', 'chess'), 'game.md')

      self.generate_title game

      @game = {
        "white" => white,
        "black" => black,
        "result" => game.result,
        "whiteresult" => game.result == "1-0" ? '1' : game.result == "0-1" ? '0' : 'Unknown',
        "blackresult" => game.result == "0-1" ? '1' : game.result == "1-0" ? '0' : 'Unknown',
        "event" => game.tags['Event'],
        "round" => game.tags['Round'],
        "boards" => generate_boards(game),
      }
    end

    def generate_boards(game)
      position = PGN::Position.start
      boards = []
      ply = 1
      game.moves.each { |move|
        position = position.move(move.notation)
        boards << {
          "fen" => position.to_fen.to_s,
          "move" => move.notation,
          "ply" => ply,
          "number" => (ply - 1) / 2 +1,
          "player" => ply % 2 == 1 ? 'w':'b',
        }
        ply = ply + 1
      }
      boards
    end

    def generate_title(game)
      white = game.tags["White"]
      black = game.tags["Black"]
      event = game.tags["Event"]
      round = game.tags["Round"]

      title = ""
      unless event.empty?
        title << event
        unless round.empty?
          title << " (round " << round << ")"
        end
        @game_subtitle = String.new(title)
        title << ": "
      end
      @game_title = white + " versus " + black
      title << @game_title

      @title = title
    end

    def generate_url(game)
      white = game.tags["White"]
      black = game.tags["Black"]
      event = game.tags["Event"]
      round = game.tags["Round"]

      @name = ""
      unless event.empty?
        @name << event.url_safe << "-"
        unless round.empty?
          @name << round.url_safe << "-"
        end
      end
      @name << white.url_safe << "-vs-" << black.url_safe << ".html"

      self.process(@name)
      @url = File.join(@dir, @name)
    end

    #def render(layouts, site_payload)
    # payload = {
    #   "page" => self.to_liquid
    # }
    # payload = payload.merge(site_payload)
    # puts payload
    # do_layout(payload, layouts)
    #end

    #def to_liquid(attr = nil)
    # self.data.merge({
    #   'content' => self.content,
    #   'game_name' => @game_name
    # })
    #end
  end

  class ChessGamePageGenerator < Generator
    def generate(site)
      repo = site.config['chess-repo']
      contents = open(repo) { |a| a.read }
      stringio = StringIO.new(contents)
      input = Archive::Tar::Minitar::Input.new(stringio)
      input.each { |entry|
        if entry.file? and entry.full_name.end_with? '.pgn'
          begin
            site.collections["chess_games"].docs << ChessGamePage.new(site, site.source, 'chess', entry)
          rescue StandardError => e
            Jekyll.logger.error e
          end
        end
      }
    end
  end
end
