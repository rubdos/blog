$(function () {
  $("#chess-board").each(function (index) {
    div = $(this)
    cfg = {
      pieceTheme: '/assets/img/chesspieces/wikipedia/{piece}.png',
      position: 'start'
    }
    board = ChessBoard(div, cfg);
    current_ply = 0;

    function do_ply(ply) {
      if(ply < 0) return false;
      if(ply > plies.length) return false;

      current_ply = ply;
      board.position(plies[ply]);

      $(".chess-move").parent("td").removeClass("active-move");
      $("#ply-" + ply).parent("td").addClass("active-move");
    }

    do_ply(plies.length - 1)

    $(".chess-move").click(function (){
      do_ply($(this).data('ply'))
    });
    $("#moves").height($("#chess-board").height());

    $(document).keydown(function(e){
      if (e.keyCode == 37) { //left
        do_ply(current_ply - 1);
        return false;
      }
      if (e.keyCode == 38) { //up
        do_ply(current_ply - 2);
        return false;
      }
      if (e.keyCode == 39) { //right
        do_ply(current_ply + 1);
        return false;
      }
      if (e.keyCode == 40) { //down
        do_ply(current_ply + 2);
        return false;
      }
    });
  });
});
