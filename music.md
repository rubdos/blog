---
layout: page
title: Music
subtitle: My annotated music
permalink: /music/
---

I play the piano, and sometimes note down music that I play.
I recently discovered [Lilypond](http://lilypond.org/),
which is a [LaTeX](https://www.latex-project.org/)-like music typesetting system.
This enabled me to put my annotations in a [Git repository at GitLab](https://gitlab.com/rubdos/music),
and automatically update this page with pdf files and midi files.
As always: patches are welcome!

<hr />

{% assign artists = site.music | group_by: "artist" %}
{% for artist in artists %}
<h3>{{ artist.name }}</h3>
{% for sheet in artist.items %}
<a href="{{ sheet.url | relative_url }}">{{ sheet.song }}</a>
{% endfor %}
{% endfor %}
