class Node {
  constructor(p, l = []) {
    this.point = p;
    this.children = l;
    this.children.sort(function (a, b) {
      a.x < b.x
    });
  }
  push(node) {
    this.children.push(node);
    this.children.sort(function (a, b) {
      a.x < b.x
    });
  }
}

class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

class Line {
  constructor(p1, p2) {
    this.p1 = p1;
    this.p2 = p2;
  }
}

function distance_squared(p1, p2) {
  return (p1.x - p2.x)**2 + (p1.y - p2.y)**2;
}

/*
 * Returns:
 *  0 when co-linear
 *  positive when c is left of ab
 *  negative when c is right of ab
 */
function orientation(a, b, c) {
  let det = a.x*(b.y - c.y) - a.y*(b.x - c.x) + b.x*c.y - b.y*c.x;
  return det;
}

function is_inside_triangle(a, b, c, x) {
  let o1 = orientation(a, b, x);
  let o2 = orientation(b, c, x);
  let o3 = orientation(c, a, x);
  if (o1 >= 0 && o2 >= 0 && o3 >= 0)
    return true;
  if (o1 <= 0 && o2 <= 0 && o3 <= 0)
    return true;
  return false;
}

function calculate_triangulated_hull(points) {
  if(points.length < 4) {
    return points;
  }
  let to_delete = [];
  for(a in points) {
    for(b in points) {
      for(c in points) {
        for(x in points) {
          if (is_inside_triangle(points[a], points[b], points[c], points[x])) {
            to_delete.push(int(x));
          }
        }
      }
    }
  }

  return points.filter(function (el, i, obj) {
    return !to_delete.includes(i)
  });
}

function graham_scan(points) {
  // If we have 3 points, we still want to return in cco.
  if (points.length < 3) {
    return points;
  }

  let leftmost = points[0];
  for (i in points) {
    let p = points[i];
    if (p.x < leftmost.x ||
      (p.x == leftmost.x && p.y < leftmost.y)) {
      leftmost = p;
    }
  }

  let ordered = points.slice();
  let leftmost_index = ordered.indexOf(leftmost);
  ordered.splice(leftmost_index, 1);
  ordered.sort(order_with_respect_to(leftmost));
  ordered.push(leftmost);

  let hull = [];

  for(let i=0; i < ordered.length; ++i) {
    let p = ordered[i];
    while (hull.length >= 2 && orientation(hull[hull.length - 2], hull[hull.length - 1], p) > 0) {
      hull.pop();
    }
    hull.push(p);
  }

  return hull;
}

function chan_hull(points) {
  if(points.length < 3) return points;
  // First group in groups of size m = 4
  let H = 2;
  while(H < points.length) {
    H = H*H;
    let hull = chan_hull_inner(points, H);
    if(hull != false) return hull;
  }
  console.error("Hull did not calculate");
  return [];
}

function chan_hull_inner(points, H) {
  let groups = [];
  for(let i = 0; i < points.length; i += H) {
    groups.push(points.slice(i, Math.min(points.length, i + H)));
  }
  let hulls = groups.map(graham_scan);

  let leftmost = points[0];
  for (i in points) {
    let p = points[i];
    if (p.x < leftmost.x ||
      (p.x == leftmost.x && p.y < leftmost.y)) {
      leftmost = p;
    }
  }

  // Now perform Jarvis march
  let hull = [leftmost];
  for(let k = 0; k < H; ++k) {
    let next = undefined;
    for(i in groups) {
      let group = groups[i];

      // Find the minimum with respect to the previous two points
      let tangents = calculate_tangents(group, hull[hull.length - 1]);
      for(j in tangents) {
        let tangent = tangents[j];
        if(next == undefined || orientation(hull[hull.length - 1], next, tangent.p1) > 0) {
          if(tangent.p1 == hull[hull.length - 1]) continue;
          // The newly found point is lower than the previous lowest.
          next = tangent.p1;
        }
      }
    }
    if(next == hull[0]) {
      return hull;
    }
    hull.push(next);
  }

  return false;
}

function bsearch(haystack, compare) {
  let start = 0; // Inclusive
  let end = haystack.length; // Exclusive

  while(end-start > 1) {
    let mid = ceil((start + end)/2);
    if(compare(mid)) {
      end = mid;
    } else {
      start = mid;
    }
  }
  return start;
}

// Requires the polygon to be convex, and given in cco
// We consider the border of the polygon part of "inside"
// Runtime: O(log n)
function is_inside_convex_polygon(polygon, point) {
  if (polygon.length < 3) {
    return false;
  }

  let fixpoint = polygon[polygon.length - 1];
  let i = bsearch(polygon, function(el) {
    return orientation(fixpoint, polygon[el], point) >= 0;
  });
  if(i + 2 > polygon.length) return false;

  return is_inside_triangle(fixpoint, polygon[i], polygon[i+1], point);
}

// Calculate both tangents of the point to the polygon.
// Method: calculate the hull of the set {p, p_1, p_2},
// and the segments connecting with p are the tangents.
// Runtime: log(n)
function calculate_tangents(polygon, point) {
  if(polygon.length < 2) {
    return [];
  }
  // First step: split polygon in two
  let fixpoint = polygon[0];
  let is_inverse = orientation(fixpoint, point, polygon[1]) < 0;

  let splitpoint = bsearch(polygon, function(el) {
    return (orientation(fixpoint, point, polygon[el]) < 0) != is_inverse;
  });

  // items < splitpoint are right halve
  let right = polygon.slice(0, splitpoint + 1);
  let left = polygon.slice(splitpoint + 1);
  left.push(polygon[0]);
  if(is_inverse) {
    let temp = right;
    right = left;
    left = temp;
  }

  // Tangent at the right is where orientation becomes right
  let tangent_right = bsearch(right, function(el) {
    let prev = left[left.length - 2];
    if (el != 0) {
      prev = right[el - 1];
    }
    return orientation(prev, right[el], point) >= 0;
  });

  // Tangent at the left is where orientation becomes left
  let tangent_left = bsearch(left, function(el) {
    let prev = right[right.length - 1];
    if (el != 0) {
      prev = left[el - 1];
    }
    return orientation(prev, left[el], point) < 0;
  });

  let tangents = [];

  if (right.length > tangent_right) {
    tangents.push(new Line(right[tangent_right], point));
  }
  if (left.length > tangent_left) {
    tangents.push(new Line(left[tangent_left], point));
  }
  return tangents;
}

// Tests:
// calculate_tangents([new Point(0,0), new Point(1,-1), new Point(2,0), new Point(2,1), new Point(1,2), new Point(0, 1)], new Point(0.5,4))
// should return (0, 1) and (2,1)

function order_with_respect_to(basepoint) {
  return function(a, b) {
    return orientation(basepoint, a, b);
  }
}
