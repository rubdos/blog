var points = [];
var hull = [];

function setup() {
  var canvas = createCanvas(640, 480);
  canvas.parent('sketch-holder');
  fill('black');
}

function draw() {
  background(200);
  if (hull.length > 0) {
    let previous = hull[0];
    for (i in hull) {
      let p = hull[i];
      fill('green');
      stroke('green');
      ellipse(p.x, p.y, 9, 9);
      stroke('black');
      line(previous.x, previous.y, p.x, p.y);
      previous = p;
    }
    // Round hull.
    line(previous.x, previous.y, hull[0].x, hull[0].y);
  }

  // Draw the points
  let to_draw = points;
  for (i in to_draw) {
    let p = to_draw[i];
    if (is_inside_convex_polygon(hull, p)) {
      fill('red');
      stroke('red');
    } else {
      fill('blue');
      stroke('blue');
    }
    ellipse(p.x,p.y,4,4);
  }
}


function mousePressed() {
  points.push(new Point(mouseX,mouseY));
  hull = chan_hull(points);
}

$(document).ready(function() {
  $("#reset").click(function(event) {
    points = [];
    hull = [];
  });
});
