var points = [];
var hull = [];
var ordered = [];

function setup() {
  var canvas = createCanvas(640, 480);
  canvas.parent('sketch-holder');
  fill('black');
}

function draw() {
  background(200);
  for (i in hull) {
    fill('green');
    stroke('green');
    ellipse(hull[i].x,hull[i].y,9,9);
  }

  // Draw the points
  for (i in points) {
    if (i == 3) {
      if (is_inside_triangle(points[0], points[1], points[2], points[3])) {
        // Set red
        fill('red');
        stroke('red');
      } else {
        // Set blue
        fill('blue');
        stroke('blue');
      }
    } else {
      fill('black');
      stroke('black');
    }
    ellipse(points[i].x,points[i].y,4,4);
  }

  // Now paint the ordering
  if (points.length > 0) {
    fill('black');
    stroke('black');
    let prev = points[0];
    for (i in ordered) {
      line(prev.x, prev.y, ordered[i].x, ordered[i].y);
      prev = ordered[i];
    }
  }
}


function mousePressed() {
  points.push(new Point(mouseX,mouseY));
  hull = calculate_triangulated_hull(points);
  ordered = points.slice();
  ordered.splice(0,1);
  ordered.sort(order_with_respect_to(points[0]));
}

$(document).ready(function() {
  $("#reset").click(function(event) {
    points = [];
    hull = [];
    ordered = [];
  });
});
