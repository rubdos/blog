var tree = tangled_tree();
var untangled = [];
var x_longest_increasing_sequence = [];
var x_longest_decreasing_sequence = [];
var selected = tree;

var node_diameter = 15;

var width = 640;
var height = 480;

function setup() {
  var canvas = createCanvas(640, 480);
  canvas.parent('sketch-holder');
  fill('black');
  invalidate();
}

function tangled_tree() {
  let root = new Node(new Point(320.0, 10.0));

  let right = new Node(new Point(400, 100));
  right.push(new Node(new Point(260, 200)))
  right.push(new Node(new Point(480, 400)))

  let left = new Node(new Point(240, 140));
  left.push(new Node(new Point(180, 260)));
  left.push(new Node(new Point(360, 300)));

  root.push(right);
  root.push(left);

  return root;
}

function draw_tree(subtree, colour = ['black', 'red'], par = null, level = 0) {
  let point = subtree.point;
  if (subtree == selected) {
    fill('blue');
    stroke('blue');
  } else {
    if (level % 2 == 0) {
      fill(colour[1]);
      stroke(colour[1]);
    } else {
      fill(colour[0]);
      stroke(colour[0]);
    }
  }
  strokeWeight(0);
  ellipse(point.x, point.y, node_diameter, node_diameter);

  if (par) {
    stroke(colour[0]);
    strokeWeight(5);
    line(point.x, point.y, par.x, par.y);
  }

  for (i in subtree.children) {
    draw_tree(subtree.children[i], colour, point, level + 1);
  }
}

function draw() {
  background(200);
  draw_tree(untangled, ['grey', 'grey']);
  draw_tree(tree);

  strokeWeight(2);
  // Draw longest increasing subsequence wrt. the x-axis.
  stroke('red');
  for (let i=0; i < (x_longest_increasing_sequence.length - 1); ++i) {
    let a = x_longest_increasing_sequence[i].point;
    let b = x_longest_increasing_sequence[i + 1].point;
    line(a.x, a.y, b.x, b.y);
  }
  stroke('aqua');
  for (let i=0; i < (x_longest_decreasing_sequence.length - 1); ++i) {
    let a = x_longest_decreasing_sequence[i].point;
    let b = x_longest_decreasing_sequence[i + 1].point;
    line(a.x, a.y, b.x, b.y);
  }

  // Draw number
  let ordered = yield_tree(untangled);
  noStroke();
  fill('white');
  textSize(12);
  for (i in ordered) {
    text(i.toString(), ordered[i].point.x, ordered[i].point.y)
  }
  // Draw number
  ordered = yield_tree(tree);
  noStroke();
  fill('white');
  textSize(12);
  for (i in ordered) {
    text(i.toString(), ordered[i].point.x, ordered[i].point.y)
  }
}

var dragging = false;

function find_node(subtree, evaluator) {
  if (evaluator(subtree)) {
    return subtree;
  }

  for (i in subtree.children) {
    let sub = find_node(subtree.children[i], evaluator);
    if (sub) return sub;
  }

  return null;
}

function findNodeAt(x, y) {
  let r_squared = (node_diameter/2)**2;
  let p = new Point(x, y);

  return find_node(tree, function(node) {
    let d_sq = distance_squared(node.point, p);
    return d_sq <= r_squared;
  });
}

function mousePressed() {
  console.log("Clicked", mouseX, mouseY);
  if (mouseButton === RIGHT) {
    let p = new Node(new Point(mouseX, mouseY));
    selected.push(p);
    return false;
  } else if (mouseButton === LEFT) {
    if (!dragging) {
      let node = findNodeAt(mouseX, mouseY);
      if (node) {
        selected = node;
        dragging = true;
      }
    }
  } else if (mouseButton === CENTER) {
    let node = findNodeAt(mouseX, mouseY);
    if (node && node != tree) {
      let par = find_node(tree, function(sub) {
        return sub.children.indexOf(node) != -1;
      });
      par.children.splice(par.children.indexOf(node), 1);
    }
  }
}

function mouseReleased() {
  dragging = false;
  invalidate();
}

function mouseDragged() {
  if (dragging) {
    selected.point.x = mouseX;
    selected.point.y = mouseY;
  }
}

// Javascript translation of
// https://en.wikipedia.org/wiki/Longest_increasing_subsequence
function longest_increasing_subsequence(sequence, cmp) {
  let p = [];
  let m = [];
  let l = 0;
  for (i in sequence) {
    // Search for largest positive j <= l
    // such that sequence[m[j]] < x[i]
    let new_l = bsearch(m, function(mid) {
      return cmp(sequence[m[mid]], sequence[i]);
    }) + 1; // +1 since bsearch returns the index, not the len

    p[i] = m[new_l - 1];
    m[new_l] = i;

    if (new_l > l) {
      l = new_l;
    }
  }

  s = [];
  k = m[l];
  for (let i = l-1; i >=0; i--) {
    s.push(sequence[k]);
    k = p[k];
  }

  s.reverse();
  return s;
}

function yield_tree(tree) {
  return filter_tree(tree, function() { return true });
}

function filter_tree(subtree, condition, level = 0) {
  subtree.level = level;
  let list = []
  for (i in subtree.children) {
    list.push(...filter_tree(subtree.children[i], condition, level + 1));
  }
  if (condition(subtree)) {
    list.push(subtree);
  }
  return list;
}

function map_tree(subtree, fn) {
  let children = [];
  for (i in subtree.children) {
    let sub = map_tree(subtree.children[i], fn);
    sub.original = subtree.children[i];
    children.push(sub);
  }
  return new Node(fn(subtree), children);
}

function invalidate() {
  let seq = filter_tree(tree, function(x) {
    return (x.level % 2) == 0;
  });

  x_longest_increasing_sequence = longest_increasing_subsequence(seq, function(a, b) {
    return a.point.y < b.point.y;
  });
  x_longest_decreasing_sequence = longest_increasing_subsequence(seq, function(a, b) {
    return a.point.y > b.point.y;
  });

  untangled = untangle(tree);
  console.log(untangled);

  let r = x_longest_increasing_sequence.length;
  let s = x_longest_decreasing_sequence.length;
  $("#increasing-count").text(r);
  $("#decreasing-count").text(s);
  $("#n").text(seq.length);
  $("#r-s").text(r*s);
  $("#max-moved").text((int)(seq.length - Math.sqrt(seq.length/2)));
}

function untangle() {
  // Let S be a color class, postorder traversed
  let seq = filter_tree(tree, function(x) {
    return (x.level % 2) == 0;
  });

  // Let R be a largest ordered subset R ⊆ S,
  // be it increasing or decreasing.
  let R1 = longest_increasing_subsequence(seq, function(a, b) {
    return a.point.y < b.point.y;
  });
  let R2 = longest_increasing_subsequence(seq, function(a, b) {
    return a.point.y > b.point.y;
  });

  let is_increasing = true;
  let R = R1;
  if (R1.length > R2.length) {
    is_increasing = false;
    R = R1;
  } else {
    R = R2;
  }

  console.log("Is increasing", is_increasing);
  console.log("R_y", R.map(function(r) { return r.point.y }))
  console.log("seq", seq);

  // Populate the coordinates of S in T'
  let s_y = new Array(seq.length);
  // First populate those in R
  let prev = -1;
  for (let i = 0; i < seq.length; ++i) {
    if(R.indexOf(seq[i]) != -1) {
      s_y[i] = seq[i].point.y;
      for(let j = prev + 1; j<i; ++j) {
        console.log("j", j);
        s_y[j].next = i;
      }
      prev = i;
    } else {
      s_y[i] = {
        'prev': prev,
        'next': -1,
      };
    }
  }

  // Now fill the others
  for (i in seq) {
    if(R.indexOf(seq[i]) != -1) continue;
    let prev = s_y[i].prev;
    let next = s_y[i].next;
    if(prev == -1) {
      let dy = (next - i)*40;
      if(is_increasing) dy *= -1;
      s_y[i] = s_y[next] + dy;
    } else if (next == -1) {
      let dy = (i - prev)*40;
      if(!is_increasing) dy *= -1;
      s_y[i] = s_y[prev] + dy;
    } else {
      let dy = (s_y[next]-s_y[prev])/(next - prev);
      dy *= i - prev;
      s_y[i] = s_y[prev] + dy;
    }
  }
  console.log("s_y", s_y);

  // Move all vertices in R to the y-axis
  let T_prime = map_tree(tree, function(node) {
    let seq_idx = seq.indexOf(node);
    if (R.indexOf(node) != -1) { //Note: this indexOf *could* be done using bsearch
      return new Point(0, node.point.y);
    } else if (seq_idx != -1) {
      console.log(s_y[seq_idx]);

      return new Point(0, s_y[seq_idx]);
    } else {
      return new Point(node.point.x, node.point.y);
    }
  });

  let post_ordered = yield_tree(T_prime);
  for(let i = 0; i < post_ordered.length; ++i) {
    el = post_ordered[i];
    if(el.level % 2 != 0) {
      // Not in S
      // Move inbetween highest child and lowest parent
      if(i == 0) {
        el.point.y = post_ordered[1].point.y;
        if(!is_increasing) {
          el.point.y += 40;
        } else {
          el.point.y -= 40;
        }
      } else if (i == post_ordered.length - 1) {
        el.point.y = post_ordered[post_ordered.length - 1].point.y;
        if(is_increasing) {
          el.point.y += 40;
        } else {
          el.point.y -= 40;
        }
      } else {
        let prev = post_ordered[i-1];
        let next = post_ordered[i+1];
        el.point.y = (prev.point.y + next.point.y)/2
      }
    } else {
      // In S. Move all children (in opposite y-ordering)
      let children = el.children.concat().sort(function(a, b) { return a.point.y < b.point.y })
      if(!is_increasing) {
        children.reverse();
      }
      for(let i = 0; i < children.length; ++i) {
        let child = children[i];
        let dy = abs(child.point.y - el.point.y);
        child.point.x = pow(dy, 1.2);
      }
    }
  }

  if($("#lemma1").prop("checked")) {
    // Now unmove those that were in R
    let epsilon = 1; // Assumption. XXX
    let X = max(...R.map(function(a) { return a.point.x }));
    console.log("X", X);

    for(let i = 0; i < post_ordered.length; ++i) {
      let el = post_ordered[i];
      if(R.indexOf(el.original) == -1) {
        el.point.x *= X;
        el.point.x /= epsilon;
      } else {
        el.point.x = el.original.point.x;
      }
    }
  }

  return T_prime;
}

function clean_untangle() {
  tree = untangled;

  // Now rescale such that everything is visible.
  elements = yield_tree(tree);
  let x = elements.map(function (e) { return e.point.x });
  let y = elements.map(function (e) { return e.point.y });
  let max_x = max(...x);
  let max_y = max(...y);
  let min_x = min(...x);
  let min_y = min(...y);
  let margin = 20;

  let scale_x = (width-2*margin)/(max_x - min_x);
  let scale_y = (height-2*margin)/(max_y - min_y);

  // First clamp minima
  for(i in elements) {
    let el = elements[i];
    el.point.x -= min_x;
    el.point.y -= min_y;

    el.point.x *= scale_x;
    el.point.y *= scale_y;

    el.point.x += margin;
    el.point.y += margin;
  }
}

$(document).ready(function() {
  $("#reset").click(function(event) {
    tree = tangled_tree();
    invalidate();
  });
  $("#untangle").click(function () {
    clean_untangle();
    invalidate();
  });
  $("#lemma1").change(function() {
    console.log("#lemma1 clicked");
    invalidate();
  });
});
