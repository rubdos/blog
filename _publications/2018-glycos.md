---
title: "Glycos: the basis for a peer-to-peer, private online social network"
layout: page
date: 2019-04-16
type: proceedings
preprint: https://etrovub.gitlab.io/smartnets/ruben-papers/ifip-ss-2018.pdf
doi: 10.1007/978-3-030-16744-8_9
doi_text: The final publication is available at Springer via
authors:
  - Ruben De Smet
  - Ann Dooms
  - An Braeken
  - Jo Pierson
tags:
  - online social network
  - peer-to-peer
  - privacy by design
  - privacy
---

Typical Web&nbsp;2.0 applications are built on abstractions, allowing developers to rapidly and securely develop new features.
For decentralised applications, these abstractions are often poor or non-existent.
By proposing a set of abstract but generic building blocks for the development of peer-to-peer (decentralised),
private online social networks, we aim to ease the development of user-facing applications.
Additionally, an abstract programming system decouples the application from the data model,
allowing to alter the front-end independently from the back-end.
The proposed proof-of-concept protocol is based on existing cryptographic building blocks,
and its viability is assessed in terms of performance.
