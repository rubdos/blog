---
title: "Private Electronic Road Pricing using Bulletproofs with Vector Commitments"
layout: page
date: 2023-09-13
type: journal
doi: 10.1109/TDSC.2023.3314867
doi_text: The final publication is published through IEEE Transactions on Dependable Computing via
full_text: https://ieeexplore.ieee.org/document/10250959/
authors:
  - Ruben De Smet
  - Kris Steenhaut
  - An Braeken
tags:
  - bulletproofs
  - vector commitment
  - privacy by design
  - privacy
---

We present a novel approach to privacy preserving electronic road pricing (ERP) based on on-board units (OBUs) and zero-knowledge proofs (ZKPs), and without any need for tamper-proof elements.
Since our approach is software-only and protocol-enforced, it can be rapidly deployed on off-the-shelve or even pre-existing hardware, such as a smartphone or the on-board computer of a car.
In addition, communication complexity is only logarithmic in function of route length, such that even for short routes the communication cost of the protocol is lower than the cost of naively transmitting the clear text route.
Our implementation proves the construction to be computationally practical, especially for the verifier.
Since the scheme is based on ZKPs, no unnecessary information gets leaked.
At the basis of the scheme lies Bulletproofs, which is modified to provide native support for Pedersen vector commitments with logarithmic impact on proof size.
