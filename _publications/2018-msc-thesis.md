---
title: "Glycos: an extensible, resilient and private peer-to-peer online social network"
layout: page
date: 2018-06-01
type: thesis
full_text: https://rubdos.gitlab.io/msc-thesis/main.pdf
# doi: 10.1007/978-3-030-16744-8_9
# doi_text: The final publication is available at Springer via
authors:
  - Ruben De Smet
  - Ann Dooms
  - Jo Pierson
tags:
  - online social network
  - peer-to-peer
  - privacy by design
  - privacy
---

Online privacy typically comes in two forms.
At one hand, users can typically choose with whom of their connections to share information,
and have plenty of *social privacy controls*.
The so-called \textit{privacy problem} is more about *institutional privacy*,
whereby the service provider fails to securely store users' data,
be it on purpose or not&nbsp;(danah boyd & Hargittai, 2010).
When on purpose, these data are often *mined* for profit through resale of profiles; often called profiling.

One way of giving back this institutional privacy to citizens,
is by taking away the institution as a whole, by decentralising the application.
Care has to be taken as not to make potential "re-centralisation" possible,
as is happening to email,
where a few large email server providers take up large portions of worldwide email traffic.
Mailchimp, a large email marketing company, reports GMail having over 1.8&nbsp;billion more email delivered than Hotmail&nbsp;(Khan, 2015),
and online resources seem to suggest that both Microsoft and GMail are by far the world most popular email service providers&nbsp;(Datanyze, 2018; Lewkowicz, 2017).

By opting for a carefully designed peer-to-peer design,
the risk of this "re-centralisation" can be minimised.

Several noteworthy efforts have been made to "re-*de*centralise" online social media platforms, not only academic, also commercial and community projects.
These efforts often adapt underlying protocols on a per-feature basis,
slowing down the development process,
and often scaring off non-domain-specialist developers.
This is in contrast with how web development and mobile app development works,
where developers have APIs such as cookies, SQL (often with ORM), and REST.
These APIs offer developers an *abstract method* of reasoning about their application.

<hr/>

We explore a peer-to-peer, fully trustless, obfuscated graph-database model,
that is only readable and efficiently traversable by legitimate users.
Outsiders only learn a minimal amount of metadata,
without revealing content nor structure of the graph database.

This database model is designed as building block for development of online social media,
keeping in mind mobile-friendliness, scalability, and efficiency.
