---
layout: default
custom_js:
  - /assets/js/chessify.js
  - /assets/js/chessboard-0.3.0.min.js
custom_css:
  - /assets/css/chessboard-0.3.0.min.css
  - /assets/css/chess.css
---

<h1> {{ page.game_title }}
<small> {{ page.game_subtitle }} </small> </h1>

<table>
<tr>
  <th>White:</th>
  <td style="font-weight:bold;">
    <img src="{{ '/assets/img/chesspieces/wikipedia/wK.png' | relative_url }}" style="height:1.3em;" />
    {{ page.game.white }}
  </td>
  <td>
    {{ page.game.whiteresult }}
  </td>
</tr>
<tr>
  <th>Black:</th>
  <td style="font-weight:bold;">
    <img src="{{ '/assets/img/chesspieces/wikipedia/bK.png' | relative_url }}" style="height:1.3em;" />
    {{ page.game.black }}
  </td>
  <td>
    {{ page.game.blackresult }}
  </td>
</tr>
<tr>
  <th>Tournament/event:</th>
  <td>{{ page.game.event }} {{ page.game.round }}</td>
  <td></td>
</tr>
<tr>
  <th>Date:</th>
  <td>{{ page.date | date: '%d %B %Y' }}</td>
  <td></td>
</tr>
</table>

<hr />

<div class="row">
<div style="width:60%; float:left;">
  <div id="chess-board" style="width: 100%"></div>
</div>
<div style="margin-left:10px; float:left;overflow-y: scroll;" id="moves">
<table id="moves-table">
<tr>
{% for board in page.game.boards %}
{% if board.player == 'w' %}
<td>{{ board.number }}.</td>
{% endif %}
<td><a class="chess-move" id="ply-{{ board.ply }}" data-ply="{{ board.ply }}">{{ board.move }}</a></td> {% cycle row_cycle:'', '</tr><tr>' %}
{% endfor %}
</tr>
</table>
</div>
</div>
<script type="text/javascript">
plies = [
    'start',
    {% for board in page.game.boards %}
    '{{ board.fen }}',
    {% endfor %}
]
</script>
