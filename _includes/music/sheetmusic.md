---
layout: default
custom_js:
custom_css:
---

<h1> {{ page.song }}
<small>by <strong>{{ page.artist }}</strong> </small> </h1>

<p>
<a href="/assets/sheetmusic/{{ page.midiname }}">Download MIDI</a><br />
<a href="/assets/sheetmusic/{{ page.pdfname }}">Download PDF</a><br />
<a href="https://gitlab.com/rubdos/music">Contribute to this sheet on GitLab</a>
</p>

<embed src="/assets/sheetmusic/{{ page.pdfname }}"
	width="100%" height="500" alt="pdf" pluginspage="https://www.mozilla.org/nl/firefox/new/">
