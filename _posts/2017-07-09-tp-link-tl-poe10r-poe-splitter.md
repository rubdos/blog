---
layout: post
title: tp-link TL-POE10R Power over Ethernet splitter
date: 2017-07-09
categories:
- hardware
- sysops
author: rubdos
tags: [poe,tl-poe10r,tp-link]
---

[Power over Ethernet](https://en.wikipedia.org/wiki/Power_over_ethernet) is a technique (actually two techniques; 802.3af (PoE), and 802.3at (PoE+)) that transmits power over regular U/STP cables.
It allows for WiFi access points, routers, switches and other devices to source power from the same cable as they source data.
Several devices have built-in support for it; for example higher-end access points (for example the [Ubiquiti AP-AC-PRO](https://www.ubnt.com/unifi/unifi-ap-ac-pro/)) have it.

I was left wondering how I could power devices *without* integrated PoE support, for example an Odroid C2 (which has a generic 2.1mm plug at 5V).
I figured I needed some active 5V PoE splitter, but searching eBay for it yielded only [100Mbit-only](http://www.ebay.com/itm/Active-PoE-Splitter-Power-Over-Ethernet-48V-to-5V-2-4A-Compliant-IEEE802-3af-/191833485222?epid=546243065&hash=item2caa2aafa6:g:QXsAAOSwZ8ZW8msS) versions.
If you require Gigabit, you can find the [WT-AF-5V10W from WiFi Texas](https://find-a-poe.com/product/WT-AF-5v/).
You're only in luck if you're in the States though; shipment to Europe is around €50.

Part of the reason I was eBaying (and ali-expressing), instead of searching with regular suppliers, was that I did not believe any "brand" would manufacture this kind of device. The second reason was that I needed something cheap-ish.
Turns out there exists an option that is both relatively cheap and branded; it even turns out there are multiple options:

- D-Link has their [DPE-301GS](http://us.dlink.com/products/business-solutions/1-port-gigabit-poe-splitter/), which was around €30.
- TP-Link has their [TL-POE10R](http://www.tp-link.com/us/products/details/cat-43_TL-POE10R.html), which retails around the same price, but I was able to get it at a decent enough rate at the distributor;
- I can't get my hands on the TRENDnet TPE-114GS, but apparently this one can plug right in an Odroid C2.

## What's in the box?

The [TL-POE10R web page](http://www.tp-link.com/us/products/details/cat-43_TL-POE10R.html) is not very verbose of what this product is about, which is mainly the reason I am writing this post; both as a reminder for myself, and as a guide for others.

Long story short:

- The device itself, with Gigabit+PoE-in, Gigabit-out, power out;
- A power chord;
- One Cat5e cable (nice!).

In more detail: the power connector and chord are standard centre-positive [barrel jacks](https://cdn.sparkfun.com//assets/parts/7/3/6/3/11476-01.jpg), and the chord is a male-male version.
The device itself has a voltage-selector: 5V, 9V, and 12V are your options. The manual states a maximum power output of 10W.

This makes it easy to power any TP-Link consumer device on PoE, as all those have a female barrel jack.
I also tested powering a Ubiquiti EdgeRouter X with it in 12V mode; worked perfectly.

I created a few barrel jack 3.5mm to 2.1mm cables, and the Odroid C2 boots perfectly! Hooray!
