---
layout: post
title: Pro-bono DRM
date: 2018-10-27 11:25 +0200
author: Ruben De Smet
---

Professor [Jaap-Henk Hoepman](https://blog.xot.nl/) ([Twitter](https://twitter.com/xotoxot/)) recently wrote a blog post stating that
[smart algorithms won't ever be used in citizens favour, only to collect money in the most efficient way possible](https://blog.xot.nl/2018/10/22/moeilijker-kunnen-we-het-niet-maken-wel-minder-leuk/),
"a lack of privacy, and always ever smarter algorithms: never will these be used in our advantage".

## [#ikweiger](https://twitter.com/search?q=%23ikweiger): I refuse

This is yet again reflected in the recent [#ikweiger Twitter madness](https://twitter.com/search?q=%23ikweiger),
when the commission of internal affairs of Belgium decided to allow storing fingerprints on electronic identity cards,
allegedly to fight identity fraud.

This made me wonder --- shower-thought-style --- about techniques to improve (rather than destroy) privacy on our [famous eID cards](https://en.wikipedia.org/wiki/Belgian_national_identity_card).

## Proof of age range

Certain Belgian services and laws require a person to be in a certain age range.
For example, the national railways offer a reduced tariff for young people under the age of 26,
or when buying alcohol, you have to be 18 years or older.
When asked, we are required to proof this by showing our identity card.
The inspector then compares the picture on the card with my face, and computes my age from my birth date.
While doing this, they have access to a lot more information than required:
my birth place, my sex, my name, my national registry number, and my signature.

Instead, the inspector is only interested in a single bit of information:
*is the person I am looking at right now in the legal age range?*
The clue here are zero-knowledge proofs:
there are [cryptographic protocols](https://duckduckgo.com/?q=range+proof+age+zero-knowledge)
that allow to proof your age being in a certain range.
Range proofs are most known in the cryptocurrency world,
where e.g. Monero uses it to assert valid transactions.
Even ING, the bank, is [testing out these range proofs](https://www.ibtimes.com/ing-releases-zero-knowledge-range-proof-more-efficient-blockchain-applications-2617405).
Luckily, there is nothing inherent to them that mandates a block chain, and it is thus perfectly possible to implement these on an identity card.

## Identity fraud

It is supposedly difficult to counterfeit these eID cards,
but apparently, [the dark web offers fake Belgian passports, identity cards and driving licenses](http://newsmonkey.be/article/57657).

Counterfeiting becomes impossible if we *require* the digital part of the card to be checked, though.
When checked digitally, the inspector will require to view your picture through his hardware in order to check the digital signature.
This means an inspector potentially has access to your picture, and is processing and collecting your data!
We now need some kind of digital technology to manage restrictions on our personal data.

Hollywood is quite known for these kind of technologies: they use them to restrict (il)legal copying of digital media,
and call it DRM (digital restrictions management, sometimes digital rights management).
There exists a DRM technique specifically designed for our purpose:
[HDCP](https://en.wikipedia.org/wiki/High-bandwidth_Digital_Content_Protection).
This technique allows to encrypt the picture, such that only licensed displays can view it.
These licenses can be granted through a regular PKI,
and allows the citizen to protect his picture from the prying eyes of big brother.

## Going further

DRM could allow the Belgian railways to check your picture without allowing them to copy it,
and range proofs allow them to check your age without knowing your birth date.

In an ideal world, my eID card would have [a painting of Magritte](https://duckduckgo.com/?q=magritte+schaduw+der+lichten&iar=images&iax=images&ia=images).
And beyond that, not too much: the painting would be perfect to recognise it as mine.
It does not need my name, or my birth date.
This is a perfect way to use DRM pro-bono, and to use smart algorithms in our advantage.
Maybe our internal affairs commission should focus on these things,
instead of on trying to get fingerprints in a [broken eID design](https://twitter.com/philipdutre/status/1055859066197528576).
