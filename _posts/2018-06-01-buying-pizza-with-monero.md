---
layout: post
title: Buying pizza with Monero
date: 2018-06-01 09:19 +0200
---

Takeaway.com accepts Bitcoin, and [xmr.to](https://xmr.to) is a service that makes Bitcoin payments when inputted Moneroj.
So how difficult could it be to order pizza, using my precious Moneroj?
Here's my timeline.

**18:45**: friends ask whether I could pay, because they don't have the card reader.
I get a list of 4 pizze.
I recall that takeaway.com does accept Bitcoin, so I want to pay with XMR.
I recall that opening the XMR wallet takes a while, when it's been cold for a few months,
so I start searching for a public node to sync it with.

**18:55**: I run `monero-wallet-cli --daemon-address opennode.xmr-tw.org:18089`, insert my password.
The wallet starts syncing.

**19:00**: I have entered my order on takeaway.com, selected Bitcoin.
The Bitpay website opens up, they give a funky URL that starts with `bitcoin:r?=https://`.
No address, only amount is given.
Go to xmr.to, search whether I can enter the funky URL.
Wallet is still syncing, Bitpay says I have 15 minutes.

**19:05**: Friends ask whether the pizze were on their way.
I just found how to enter the funky URL in xmr.to, but have to remove the `bitcoin:r?=` part.
xmr.to amounts me for 37.01€, which is only one ct more than takeaway (!).
Monero wallet is still syncing.

**19:07**: Panic commences, wallet still syncing.
The open node is perhaps a tid slow; I press Ctrl+C because I'd like to try a automatic geo-dependent DNS name.

**19:10**: Wallet falls back to prompt! I enter the transaction, knowing that the wallet is not fully synced.
This shouldn't matter, as I did not receive any funds in the cold-period.
Might matter wrt. traceability, but not too much. I hope.
The Bitpay countdown is at T-5.

From here, things went kinda quick.
xmr.to almost immediately starts outputting the Bitcoin transaction,
and when they're done, Bitpay immediately claims I used a "low fee".
I checked with [bitcoinfees.earn.com](https://bitcoinfees.earn.com/),
the average block time for the fee that xmr.to used was 0-1, so no need for panic.

Bitpay nevertheless approves of the order, sending me an email "Your bitcoin transaction has been broadcast, but it does not yet have any confirmations."
Pizze are ordered.

In the meantime, [the Bitcoin transaction went through](https://insight.bitpay.com/tx/7111870bc3c2f778af2742ebe2d5ec5d184290a3a8d631c710f81687e030ac23).

### Conclusion

In the end, it seems like I payed more Monero fees than I payed BTC fees though, totaling a bit above 1€ in fees,
mostly because I did not recall the correct syntax for the lowest transaction fee in the CLI wallet.
Monero's network is fast enough that even the lowest transaction fee would pass easily at that time,
which would have saved me perhaps 0.5€. Meh, it was a fun experiment.

We did receive our pizze at **19:40**, just as expected.
