---
layout: post
title: Public PhD defense on June 17, 2024
date: 2024-06-03
categories:
- privacy
author: Ruben De Smet
tags:
- signal
- privacy
- circuitree
- zero-knowledge proofs
- cryptography

redirect_from:
- /phd-defense/
- /phd-defence/
---

# Announcement: Public PhD defense

On June 17th, 2024, [I](https://www.etrovub.be/people/member/about-bio/rdesmeta/) will defend my PhD thesis titled "Rapid prototyping and deployment of privacy-enhancing technologies" at the Vrije Universiteit Brussel.
My promotors are [prof. dr. ir. Kris Steenhaut](https://www.etrovub.be/people/member/about-bio/ksteenha/) and [prof. dr. An Braeken](https://researchportal.vub.be/en/persons/an-braeken),
and my jury further consists out of
[prof. dr. Jan Tobias Mühlberg](https://www.beetzsee.de/),
[dr. ing. Jorn Lapon](https://www.kuleuven.be/wieiswie/en/person/00058206),
[prof. dr. Ann Dooms](https://wids.research.vub.be/en/ann-dooms),
[prof. dr. ir. Frederik Temmermans](https://www.etrovub.be/people/member/about-bio/ftemmerm/),
[prof. dr. Bart Jansen](https://www.etrovub.be/people/member/about-bio/bjansen/),
and [prof. dr. ir. Wendy Meulebroeck](https://researchportal.vub.be/en/persons/wendy-meulebroeck).

## Practical details

The defense will take place at the Vrije Universiteit Brussel, Campus Etterbeek, in the [Promotiezaal D.2.01](https://www.vub.be/sites/default/files/2022-09/220908_58439_Bereikbaarheidsfiche_Brussels_Main_Campus_DIGITAAL.pdf) at **[🗺️ 10:00 CEST](https://www.timeanddate.com/worldclock/fixedtime.html?msg=Public+PhD+defense+Ruben+De+Smet&iso=20240617T10&p1=48&ah=2)**.

The defense will be in English, and will be tailored to a general audience.
The defense will be followed by a reception (probably at *'t Complex*) around early noon.
If you cannot attend the defense and reception, I expect many people to stay at the Complex after 18:00.
If you would like to physically attend the defense or the reception,
[please register online on https://forms.office.com/e/brf4FcD91w](https://forms.office.com/e/brf4FcD91w).

The event will additionally be live-streamed via Microsoft Teams.
If you would like to receive the link to the live stream,
either [register online](https://forms.office.com/e/brf4FcD91w),
or [let me know via any other channel]({{ 'about' | relative_url }}), e.g. by Signal.

![PhD cover]({{ 'assets/img/phd-cover.png' | relative_url }})

## Abstract

> Since its inception, the internet has quickly become a public service
> utility. The combination of its commercial exploitation, and the rather
> intimate nature of how humans actively use the internet, gives rise to
> some paradoxical situations.
>
> As a citizen of Belgium, I would probably not expect to give my name and
> phone number to a company in the United States to talk to my brother,
> 50 km up north. However, for over two billion people, this is their
> rather paradoxical reality: the company Meta, owning WhatsApp, collects
> and stores these data for their users. This cherry-picked scenario
> stands example for a wider trend in the industry.
>
> Cryptographers have worked on several privacy-enhancing technologies (PETs).
> These PETs aim to minimize the amount of personal data
> to fulfill a service for users. Although these technologies exist on
> paper, several practical issues arise. These practicalities are the
> subject of this thesis.
>
> One practical issue is the performance. PETs, especially those that run on end-user
> devices, should both be fast and require little bandwidth. We study how
> implementation details may lead to significant speedups or bandwidth
> savings. Specifically, we devise a zero-knowledge proof (ZKP) tailored to electronic road pricing (ERP).
> ERP is a privacy-sensitive topic, and our ERP system achieves
> some notable performance improvements over preexisting proposals.
>
> A second practical issue is the challenging nature of implementing
> PETs. We present
> "[Circuitree](https://doi.org/10.1109/ACCESS.2022.3153366)" and "[Whisperfish](https://gitlab.com/whisperfish/whisperfish/)", to study how to bring PETs to an actual
> application. Circuitree is a high-level framework to tailor ZKPs to specific
> scenarios, using a bespoke logic programming language. The language is
> designed such that the resulting ZKP is highly efficient.
>
> Whisperfish is effectively a reimplementation of the Signal instant
> messaging client, and allows to present in detail how Signal deploys
> their PETs to users.
> All ideas put forward in this thesis were evaluated by means of their
> implementation in the [Rust programming language](https://www.rust-lang.org/).

## Bibliography

These publications have lead to the work in this thesis:

**De Smet, R.**, [Blancquaert, R.](https://researchportal.vub.be/en/persons/robrecht-blancquaert), [Godden, T.](https://researchportal.vub.be/en/persons/tom-godden), [Steenhaut, K.](https://www.etrovub.be/people/member/about-bio/ksteenha/), & [Braeken, A.](https://researchportal.vub.be/en/persons/an-braeken) (2024).[Armed with Faster Crypto: Optimizing Elliptic Curve Cryptography for ARM Processors. Sensors, 24(3), Article 3.](https://doi.org/10.3390/s24031030)

**De Smet, R.**, [Steenhaut, K.](https://www.etrovub.be/people/member/about-bio/ksteenha/), & [Braeken, A.](https://researchportal.vub.be/en/persons/an-braeken) (2023). [Private Electronic Road Pricing using Bulletproofs with Vector Commitments. IEEE Transactions on Dependable and Secure Computing, 1–13.](https://doi.org/10.1109/TDSC.2023.3314867)

[Godden, T.](https://researchportal.vub.be/en/persons/tom-godden), **De Smet, R.**, [Debruyne, C.](https://chrdebru.github.io/), [Vandervelden, T.](https://thvdveld.be/), [Steenhaut, K.](https://www.etrovub.be/people/member/about-bio/ksteenha/), & [Braeken, A.](https://researchportal.vub.be/en/persons/an-braeken) (2022). [Circuitree: A Datalog Reasoner in Zero-Knowledge. IEEE Access, 10, 21384–21396.](https://doi.org/10.1109/ACCESS.2022.3153366)

[Vandervelden, T.](https://thvdveld.be/), **De Smet, R.**, [Steenhaut, K.](https://www.etrovub.be/people/member/about-bio/ksteenha/), & [Braeken, A.](https://researchportal.vub.be/en/persons/an-braeken) (2022). [SHA3 and Keccak variants computation speeds on constrained devices. Future Generation Computer Systems, 128, 28–35.](https://doi.org/10.1016/j.future.2021.09.042)

I would like to thank my co-authors for their contributions to these articles.
I would also like to thank Matti, direc85, for his contributions to Whisperfish.
