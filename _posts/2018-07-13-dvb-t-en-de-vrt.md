---
layout: post
title: DVB-T en de VRT
date: 2018-07-13 10:14 +0200
author: Ruben De Smet
---

*(article in Dutch)*

Er is nogal commotie ontstaan rond de afschaffing van de DVB-T diensten aangeboden door VRT.
Kort gezegd: er zijn verschillende manieren om de VRT kanalen te ontvangen.
Eén van die technieken is DVB-T. Dat werkt zonder abonnement of restricties,
met een erg eenvoudige ontvanger die u vermoedelijk al bezit.
In mei dit jaar [kondigde VRT aan te zullen stoppen met DVB-T uitzendingen](https://www.vrt.be/nl/aanbod/kijk-en-luister/radio-luisteren/dvbt/) vanaf 1 december.
Wist u überhaupt dat u VRT kon kijken zonder abonnement?

# Wat is DVB-T?

DVB (nog even zonder -T) is de technologie die we *de facto* in Europa gebruiken om video (en ook soms radio) op een digitale manier uit te zenden;
DVB staat dan ook letterlijk voor "Digital Video Broadcasting".
Telenet gebruikt DVB-C (met de C van kabel).
TV Vlaanderen gebruikt DVB-S(2) (met de S van satelliet).
DVB-T(2) is vooral populair in het buitenland, waar het bijvoorbeeld in het Verenigd Koninkrijk het standaard medium is.
Merk op dat in het VK vaak betaalzenders over DVB-T gaan; zij hebben niet zo'n sterk uitgewerkt kabelnetwerk als wij Belgen.

De drie technologieën hebben voordelen en nadelen, die een deel van het gebruiksgemak bepalen.
Zo moet u voor DVB-C (lees: Telenet en co.) een kabel uit de grond halen, wat op campings vaak niet werkt,
en voor pauzerende vrachtwagenchauffeurs nog minder.
Aan de andere kant heeft DVB-C de meeste mogelijkheden: de koperen coaxkabel kan potentieel de hoogste kwaliteit aan beeld voeren, en een enorm aantal kanalen tegelijk aanbieden.
Of uiteindelijk die kwaliteit uitgezonden wordt door de aanbieder is een andere kwestie.

![Satellietschotel op een dak](/assets/img/SuperDISH121.jpg){:align="right"}

In praktijk zijn de satellietsignalen vaak beter van kwaliteit.
Satellietsignalen zijn erg zwak als ze aankomen op aarde na hun tocht van 35786 kilometer.
Voor ontvangst heb u dus een speciale paraboolantenne nodig, die u erg precies moet richten op de satelliet die u wenst te ontvangen.
U kent die schotels wel van op een camper of op daken van huizen.
De schotel installeren kost even tijd, dus DVB-S is geen oplossing om de pauzes van truckchauffeurs te vullen.

Dat is waar DVB-T ten tonele verschijnt.
DVB-T gebruikt aardse antennes (de T van terrestriëel).
Deze antennes zender omnidirectioneel uit, en kan u dus vanuit alle richtingen ontvangen.

![Stofzuiger antenne](/assets/img/stofzuiger-antenne.jpg){:width="30%" align="left"}

Aangezien de signalen van de zenders redelijk sterk zijn kan u bijna elk metalen object gebruiken als ontvangstantenne.
Commerciële antennes bestaan, maar als u die dan monteert op een groot magnetisch stuk ijzer hebt u vaak een erg goede ontvanger,
zelfs binnenshuis.
Een stofzuigerbuis bijvoorbeeld, als uw kabels niet tot buiten rijken.

![Jupiler antenne](/assets/img/jupiler-antenne.jpg){:width="30%" align="right"}

Een stofzuiger installeren voor dat uurtje televisie per week is nogal omslachtig.
Momenteel gebruik ik dan ook een veel handigere antenne gebaseerd op twee blikjes Jupiler (eender welk ander magnetisch blikje werkt ook).
Zo'n "cantenna" werkt overigens vaak beter dan de goedkopere commerciële DVB-T antennes.

Voor truckchauffeurs en kampeerders is DVB-T dus echt de enige oplossing om televisie te kijken.

# Televisie via internet (VRT-NU)

Ik beweer de enige oplossing, inderdaad.
Met het VRT-NU platform kan u tegenwoordig televisie kijken via internet.
Onze campers hebben misschien (gratis of betalend) WiFi op hun camping,
maar dat lost het probleem van de truckers nog niet op.
Zij kunnen een erg duur 4G abonnement nemen (video streaming neemt relatief veel data),
als er op de plekken waar ze pauzeren al 4G dekking is.

Maar er is nog een veel fundamenteler probleem met VRT-NU.
VRT versleutelt hun live streams met DRM ("digital restriction management", soms ook "digital rights management").

![DRM in Firefox](/assets/img/drm-screen.png){:width="98%"}

Volgens de makers draait deze technologie om het beschermen van de content tegen ongeautoriseerde kopieën.
In praktijk werkt DRM niet: u kan niet iemand de sleutel én het slot geven, en verwachten dat die enkel het slot opent wanneer het u uitkomt.

DRM verhindert veel zaken op een praktische én ideologische manier.
Zo is het bijvoorbeeld illegaal die sleutel te zoeken en gebruiken op de wijze dat het u past, *zelfs al is er geen andere legale hindering*.
Wilt u graag uw opnames van uw Telenet Digicorder overzetten naar een harde schijf, of een back-up maken?
Jammer, dat mag niet.
Niet omdat u geen kopie mag maken van die opname; want dat mag.
Enkel omdat u --- om die kopie te bekijken --- de DRM zou moeten omzeilen.
Wilt u graag VRT-NU opnemen naar uw computer?
Enkel omdat er DRM rond zit mag u dat dus niet doen, net omdat u die moet omzeilen om te kunnen kijken.
Dankzij DRM moet u dansen naar de pijpen van Telenet --- of nu ook de VRT.
Dàt is uiteindelijk waar DRM om draait; *de dienstverlener wil de volledige controle over u, de consument*.

Wanneer u dus op die knop "Enable DRM", of "DRM inschakelen" klikt in uw browser zegt u dus impliciet tegen VRT of welke andere partij dan ook,
"Goed, jullie hebben nu de controle over mijn computer, en over wat ik kan doen met de video waar ik voor betaal."

Dat is nu uiteindelijk de reden dat uw Chromecast geen VRT-NU kan spelen: VRT heeft hen simpelweg niet "de toestemming" gegeven.
Voor mijn part doen de commerciële omroepen wat ze willen.
Tegen hen kan ik immers "nee" zeggen.
"Nee" van, "ik ben het niet eens met DRM", nee van "ik betaal jullie niet".
Maar is het aan VRT om een lijst op te stellen van apparaten die "toegelaten" zijn?
Wat met de apparaten die ik als burger zelf bouw?
Mijn "cantenna", met mijn eigen gebouwde software, staat niet op hun beperkte lijstje,
maar ik betaal nu wel voor een dienst die ik niet kan gebruiken.

Openbare omroep hoort geen digitale restricties te implementeren.
Er is immers al betaald om hun inhoud te mogen ontvangen, opnemen en bekijken.

<!--
# DVB-T2

Er is nog een veel minder besproken (en commercieel) probleem.
Een paar maanden voor VRT hun stopzetting van de DVB-T diensten aankondigde,
begon TV Vlaanderen met hun eigen DVB-T2 (Full HD) uitzendingen "TV met een antenne".
Zij verkochten daarbij een Full HD pakket met een aantal commerciële Vlaamse zenders,
en voegden automatisch de free-to-air kanalen van VRT toe.

Nu VRT hun DVB-T afschaft vanaf 1 december, zal TV Vlaanderen een alternatief moeten bieden aan hun spiksplinternieuwe klanten.
Die verwachten immers nog steeds VRT op hun relatief goedkoop systeem.
Mogelijks zou TV Vlaanderen kunnen instaan voor een DRM-vrij uitzenden van de VRT kanalen.
Dat lost ook meteen het probleem van de truckers, doe-het-zelvers en kampeerders op,
want de enige eenmalige kost die zij moeten maken is een nieuwe ontvanger van een €50.
De antenne is overigens dezelfde, en de kwaliteit kan plots een stuk hoger.

Aan de andere kant heeft Norking België momenteel het alleenrecht op het uitzenden van VRT,
dus zullen ze niet toelaten dat TV Vlaanderen uitzendt in naam van VRT, ookal is Norking uiteindelijk wel de netwerkoperator.
-->

Ikzelf zal alvast niet gaan betalen voor een systeem dat mij verhindert TV te kijken.
Dat is gewoon absurd.
Vermoedelijk kijk ik vanaf 1 december dan maar geen TV meer.
Gelukkig heb ik nog een papieren krant.
