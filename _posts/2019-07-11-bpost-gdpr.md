---
layout: post
title: "bpost: the Belgian Facebook?"
date: 2019-07-11
categories:
- gdpr
author: rubdos
tags: [gdpr]
---

Mandatory "I am not a lawyer".

People that know me, usually also know I'm quite fond of 99.9% the GDPR.
There's about one sentence in there that annoys many people (including myself),
known as 6.1.(f).
I'm sometimes tempted to register `61fshame.org`, because of that dreadful article.
The thing reads:

> 6.1: Processing shall be lawful only if and to the extent that at least one of the following applies:  
> ...  
> (f): processing is necessary for the purposes of the legitimate interests pursued by the controller or by a third party, except where such interests are overridden by the interests or fundamental rights and freedoms of the data subject which require protection of personal data, in particular where the data subject is a child.

**TL;DR**: someone that's after your data can use it if they say it's in their "legitimate interest".
This is basically the loophole built in by the big data mining companies,
and is - as far as I know - never been tried in court yet.
Most of the data mining companies seem to hide behind the almost equally digusting 6.1.(a) "consent".
You know, the big green button on every website nowadays.

So, I would expect to see this 6.1.(f) to be abused by Silicon Valley companies,
but I never expected our national post company to hide behind it,
until Jeroen Baert posted [this interesting Tweet](https://twitter.com/jbaert/status/957632533688193024):

<blockquote class="twitter-tweet" data-lang="nl"><p lang="nl" dir="ltr">Sorry, ik kan er nog steeds niet over dat <a href="https://twitter.com/bpost_nl?ref_src=twsrc%5Etfw">@bpost_nl</a> actief manieren zit te promoten om via wat interpretatie-gegoochel van de <a href="https://twitter.com/hashtag/GDPR?src=hash&amp;ref_src=twsrc%5Etfw">#GDPR</a> toch aan direct marketing te doen. <a href="https://t.co/gj3wxDLax3">pic.twitter.com/gj3wxDLax3</a></p>&mdash; Jeroen Baert (@jbaert) <a href="https://twitter.com/jbaert/status/957632533688193024?ref_src=twsrc%5Etfw">28 januari 2018</a></blockquote>

bpost published a document - before GDPR even became active - to convince marketing companies that GDPR would be *good* for them.
I've [rehosted the PDF](https://www.rubdos.be/GDPR_Paper_2018_NL.pdf) for your convienience;
I imagine you don't want to submit your [personal details to their download-form](http://www.bpost.be/site/nl/privacy/gdpr) after what follows.

For me, it all started when I received a particular piece of marketing by (snail) mail at my new address - now after the GDPR came into force.
I had never given that address to anything marketing-related before.
I threw the original one out, but recently got another copy (thanks bpost!).

![Front of the spam](/assets/img/bpost-front.jpg)

The front of the A5-sized unwanted marketing mail contains a few possible senders.
I'll name and shame them here for you:

- [LensOnline](https://www.lensonline.be/)
- [Plopsa](https://www.plopsa.be)/[Studio100](http://studio100.com)
- [Wonderbox](https://be.wonderbox.com/)
- [smartphoto](https://www.smartphoto.be/)
- [bpost](http://www.bpost.be/)

Two clues lead to bpost being the culprit here.
One, the flyer is clearly in bpost's style.
Second, their logo literally reads "we take care of it".

![Back of the spam](/assets/img/bpost-back.jpg)

The back is *way* more interesting.
I would expect our postal company to deliver *un*addressed marketing mail without a problem.
But this thing contains "de Heer Ruben De Smet", (Dutch, "mr. Ruben De Smet"),
followed by my brand-new address.

This simple piece of paper made me wonder:

- How do they know my name and gender, and link it to the new address?
- When and how did I tell them to send me spam?

I already had a feeling how this had happened, but since GDPR, I actually had a tool to find out:
I sent a subject access request to their data privacy office.
A bit less than 30 days after, they answered me.
They informed me they pulled data **from six different computer systems**,
and supplied a PDF that lists the systems and all data they found.
These are the systems:

1. DAS: het beheersysteem van de leveringsvoorkeuren voor pakjes
  (the management system for delivery preferences for parcels);
2. Eloqua: het beheersysteem van de directmarketingcampagnes via e-mail
  (the management system of direct marketing campaigns via email);
3. Recif: het operationele systeem ter optimalisatie van de sortering op basis van de door de afzenders bezorgde gegevens
  (the operational system for optimisation of sorting based on the data delivered by senders)
4. Shipping Manager Light: het beheersysteem van de online dienst voor het creëren van voorafbetaalde etiketten voor pakketbezorging
  (the management system of the online service for the creation of prepaid parcel labels.
5. TMM: het operationele systeem voor het beheer van de leveringen van pakjes en van aangetekende zendingen
  (the operational system for the management of deliveries of packages and signed shipments.
6. UNP: het beheersysteem van de KMO-contacten
  (the management system of SMB-contacts)

I had no clue I was in bpost's direct marketing e-mail system (2. Eloqua),
but the others sound familiar.

Their email luckily also include what I wanted to know (emphasis mine):

> Met betrekking tot uw meer specifieke vraag,
> de gepersonaliseerde post die u hebt ontvangen,
> werd u als klant van de Mijn Leveringsvoorkeuren voor pakjes
> (u bent sinds 10 oktober 2018 klant van deze dienst)
> toegestuurd op basis van het *legitieme belang* van bpost om zijn huidige klanten
> (jonger dan 3 jaar)
> te informeren over gelijkaardige producten en diensten als die welke zij gebruiken.
> In dit geval was het doel van de reclame om u de mogelijkheid te bieden om een SelectPost-enquête in te vullen
> die tot doel heeft bpost gegevens en antwoorden te verstrekken om deze door te sturen naar adverteerders om gerichte advertenties te versturen
> die beantwoorden aan uw voorkeuren en interesses.

From Dutch (emphasis mine):

> Related to your more specific question,
> the personalised mail you received,
> was sent to you as a customer of the "My Delivery Preferences for parcels"
> (you have been a customer of this service as of 10 october 2018)
> based on the *legitimate interest* of bpost to inform current customers
> (since three years)
> about relevant products and services to those they use.
> In this case, the goal of the marketing was to offer you the option to file the SelectPost-enquiry,
> which targets giving bpost data and answers to spread them to advertisers to sent direct marketing advertisements,
> relevant to your preferences and interests.

bpost uses my *delivery preferences*[^1] to send me direct marketing, claiming they can do so because it's in their legitimate interest.
They don't even *try* to defend that I maybe gave consent in the details of the privacy statement.
This makes bpost a [surveillance capitalist](https://en.wikipedia.org/wiki/Surveillance_capitalism).
More than that: this means that bpost is not a postal or parcel delivery company anymore.
They deliver post and parcels *with the intent of capitalising on your data*,
just like Facebook exploits their media platforms with the intent of capitalising on your personal data.

I wonder whether other Belgian citizens had the same experience,
and I wonder whether other EU citizens had an alike expercience.
If you're interested in viewing the 15-page PDF document with all my personal data they gathered,
feel free to contact me.
There are quite a few gems left in there, but I sadly don't have time nor the energy to process it all,
let alone to push them to change.

I hope that maybe [the Belgian Data Protection Authority](https://www.dataprotectionauthority.be/) reads this and starts some kind of investigation.
It might be an interesting case to study the dreaded 6.1.(f) and have some interesting results.

[^1]: Delivery preferences don't even seem to work, I just had to go elsewhere *again*.
