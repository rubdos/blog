---
layout: post
title: 'UPDATE: my limited experience with btrfs. Snapper with qgroups'
date: 2016-12-03 12:32:51.000000000 +01:00
type: post
published: true
status: publish
categories: []
tags: []
meta:
  _edit_last: '1'
author: rubdos
---

(**Please note [this follow up post, where `qgroups` failed miserably]({% post_url 2016-12-09-update-2-btrfs-qgroups %}).**)

Yesterday, [I wrote about my laptop getting messed up by some issues using btrfs]({% post_url 2016-12-02-my-limited-experience-with-btrfs %}).
I ended up using a dd of my btrfs partition, creating a new partition, copying over all the files, and changing fstab to reference the new UUID of the partition.
Reboot, unlock cryptroot, and work on.
In the meantime, I didn't copy my snapper snapshots over, and I removed my docker images. I have now 230GB of free space, which is plenty.

I took this opportunity to learn about [`btrfs` `qgroup`s](https://btrfs.wiki.kernel.org/index.php/Quota_support);
I created two groups (2/100 and 2/200), to limit snappers disk usage.
I set 2/100 to be the parent qgroup for `/home` snapshots (with a 50GB limit) and 2/200 the qgroup for `/` at 10GB.

Seems to work for now:

```
qgroupid         rfer         excl     max_rfer     max_excl
--------         ----         ----     --------     --------
2/100       113.30GiB     97.55MiB         none     50.00GiB
2/200        97.62GiB      2.43GiB         none     10.00GiB
```

This means that my `/home` snapshot subvolumes refer to 113.30GiB of data, of which 97.55MiB is exclusively for snapshots by snapper.
My root `/` snapshot subvolumes refer to 97.62GiB, of which 2.43GiB is exclusively for the snapshots.

`/` uses so much exclusively because I hooked up pacman to snapper; it does pre and post snapshots now.
Awesome.
And snapper should automatically clean up the least important ones to comply with the qgroup limits of btrfs!

So, normally, by balance issues should be a lot less frequent now. Long live btrfs!<
