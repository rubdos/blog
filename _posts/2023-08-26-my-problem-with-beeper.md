---
layout: post
title: My problem with Beeper
date: 2023-09-07
categories:
- privacy
- signal
author: Ruben De Smet
tags:
- digital markets act
- signal
- privacy
- beeper
---

[Beeper](https://www.beeper.com/) is a tool to get all your chats in one smooth and integrated application.
It's founded by [Eric Migicovsky](https://twitter.com/ericmigi) and [Brad Murray](https://twitter.com/bradtgmurray),
known for having built the awesome [Pebble watches](https://en.wikipedia.org/wiki/Pebble_(watch)) in the past[^1].

I am the first one to admit that keeping a crazy number of chat applications and accounts around is horrible.
I am on Matrix (@rubdos:rubdos.be; I even host my own server),
Signal (I maintain [Whisperfish](https://gitlab.com/whisperfish/whisperfish/), a third-party Signal client),
IRC (Weechat),
e-mail (with multiple different accounts),
and probably a whole lot more that I don't bother to check.
Other people use iMessage, Instagram, Facebook, and even worse platforms for intimate conversation[^2].
Especially with the tendency nowadays to write every chat application in Electron (Signal Desktop and Element come to mind in my case),
this problem hogs a lot of CPU and RAM that I would rather invest in something else.

This problem is what Beeper wants to solve:
all your chats in one single interface.
They are barely the first to attempt this.
[Pidgin](https://pidgin.im/), for example, is also a unified instant messaging interface.
The [Telepathy framework](https://telepathy.freedesktop.org/wiki/) is another one.
These are instant messaging (IM) *clients* with plugins for every protocol,
and handle the integration locally on the device.

What makes Beeper unique, is the implementation of the unification.
Essentially, Beeper is a "normal" Matrix client, and all other protocols are bridged into the Matrix server.
This means that your Beeper account is accessible from all different kinds of devices,
and all of these devices will have a unified view over your IM accounts.
This is extremely convenient!

# Beeper destroys privacy for non-Beeper users

Since Beeper implements the unification on the server-side,
the properties of a chat session are reduced to the capabilities of the Matrix protocol,
and the end-to-end encryption is terminated *at the bridge side*.
This implies that Beeper effectively acts as a man-in-the-middle, breaking end-to-end encryption, putting user data of all their users in the same place.
At least, this implies that your plain text messages exist in memory on the Beeper servers, including metadata and account and session keys.
Additionally, Beeper has the goal of implementing messenger interoperability,
which [reduces properties of all protocols for all contact to the lowest-common-denominator](https://alecmuffett.com/article/14656) in cases of mixed-protocol sessions.

Let's consider the implications from the Signal point-of-view,
since Signal is quite universally considered the state-of-the-art from a privacy perspective.
For example, Signal does not know[^3] the members of groups,
because of their recent-ish [private group system](https://signal.org/blog/signal-private-group-system/) (2019).
Beeper, being a Matrix server, will need access to the decrypted group state,
and hence the Beeper server *will* know the members of any group.
Consider the implications of the above:
**being a passive member of a group with even a single Beeper user destroys group anonymity!**

This is also true for existing groups.
If you are part of a group on Signal,
and one of the members decides to try Beeper with their existing Signal account,
the Beeper bridge server will know about your group membership,
without you ever consenting.

Lastly and relatedly, there are some security and practicality concerns, not necessarily relating to privacy.
Services like Signal or WhatsApp might disagree, for [various reasons](https://gitlab.com/whisperfish/whisperfish/-/issues/398), to allow connections from Beeper.
They might change their mind over night, or you might bounce on the [spam filter](https://signal.org/blog/keeping-spam-off-signal/), or hit some [rate limiter](https://gitlab.com/whisperfish/whisperfish/-/issues/417).
There are a plethora of implicit security issues of sharing the same Beeper IP address when connecting to messaging services.

# Self-hosting Beeper "solves" the problem

I discussed this issue with Eric, when I had the privilege of meeting him in Brussels in February.
Largely, the issue boils down to your trust in the host of the Beeper bridges:
if you trust the bridges, then it does not necessarily matter that it knows about group states, the contents of messages, or the contacts you are talking to.

Even when you trust Beeper's servers, there are potential issues.
You may have trusted their servers yesterday, but today they can get seized by a government, or Beeper might get breached.
Or they might move their servers onto a malicious or curious cloud provider.

So, if you don't trust the Beeper servers, they offer you the option to [self-host](https://github.com/beeper/self-host).
However, this only solves one half of the issue.
Alice might trust their own server, but Bob is still forced to communicate with a bridge they don't know about.
Additionally, self-hosting always implies a maintenance burden, which might weaken the security of the communication.
In my experience, self-hosting and federation is not a solution to a privacy problem, nor does it give back your autonomy.
E-mail is a federated protocol and can be self-hosted.
In practice, we are all unwillingly users of Google Mail and Outlook at the same time.
Mastodon is a federated system, but in practice we all rely on mastodon.social.
In extenso, Matrix is a federated system, but in practice we all rely on Matrix.org.
You don't get autonomy nor privacy if your contacts are on a server you don't trust.

# Don't complain; suggest what's better

Honestly, I don't know.
Fragmentation of communication is a real issue,
and a difficult, maybe impossible one to solve.
I am actively forcing all my personal communication on Signal,
and my professional communication through email (Thunderbird) and Matrix ([weechat-matrix](https://github.com/poljar/weechat-matrix)).
This is not a (long term) solution, because I'm putting trust in Signal LLC, where it might not be appropriate.

The emotional or interpersonal implications of "please kindly register on Signal because I'm not used by Facebook" can be quite drastic,
and I realize it is not a procedure that most people can afford to follow.

Sadly, life is sometimes more complicated than a technological solution might make it seem like.

# Acknowledgements

I would like to explicitly thank [valldrac](https://github.com/valldrac) for his feedback on this post.

---

[^1]: I still wear my Pebble Time every day. Its wrist band broke a few weeks ago next to a moshpit, so now it's a red Pebble Time with a white wrist band.
[^2]: As I explained [during my talk about Whisperfish in Lausanne](https://video.rubdos.be/w/6KrhTEXzbPHoED1ztEomUU), I do not trust any platform except Signal for intimate conversations.  Also, wow, I speak with a really heavy Belgian accent nowadays.
[^3]: Signal private groups are of course not perfect. Network traffic analysis can uncover group structure, and this is generally a moving target.
