---
layout: post
title: Gitlab CI with IPv6 support in Docker images
date: 2018-06-23 18:06 +0200
author: Ruben De Smet
categories:
 - Linux
 - Gitlab
---

Some people might want to use [Gitlab's support for spinning up CI runners on DigitalOcean](https://about.gitlab.com/2018/06/19/autoscale-continuous-deployment-gitlab-runner-digital-ocean/),
and may want to test their application on IPv6...
Well, Docker, by default, does not do IPv6: you have to add `--ipv6=true` to the daemon, and add some CIDR that Docker may hand out (e.g. `fixed-cidr-v6='fc00::d0c:0:0:0:1/64'`).

If you're okay with link local IPv6, you can use these options in your Gitlab Bastion:

```
    MachineOptions = [
      "digitalocean-image=coreos-stable", # The DigitalOcean system image to use by default
      "digitalocean-ssh-user=core", # The default SSH user
      "digitalocean-access-token=ACCESS_TOKEN", #     Access token from Step 7
      "digitalocean-region=ams3", # The data center to spawn runners in
      "digitalocean-size=c-1vcpu-2gb", # The size (and price category) of your spawned runners
      "digitalocean-private-networking", # Enable private networking on runners
      "digitalocean-ipv6",
      "engine-opt=ipv6=true",
      "engine-opt=fixed-cidr-v6='fc00::d0c:0:0:0:1/64'",
    ]
```

The trick is to pass an extra = between engine-opt and the option itself, otherwise Docker Machine interprets `'engine-opt ipv6=true'` as a standalone option,
instead of `'engine-opt'` as option and `'ipv6=true'` as parameter.

If not, sad news, you have to wait for [Better IPv6 support from the Docker Machine](https://github.com/docker/machine/issues/1176),
because you'd have to pass your public prefix in the Gitlab config…
