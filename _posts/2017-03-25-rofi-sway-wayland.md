---
layout: post
title: Native Rofi for Wayland is in the making
date: 2017-03-25
categories:
- linux
- wayland
author: rubdos
tags: [linux, wayland, sway, rofi]
---

[Sway](http://swaywm.org) is an [i3](http://www.i3wm.org)-gaps clone,
but using [Wayland](https://wayland.freedesktop.org) instead of [X11](https://www.x.org/).
Recently, since Sway 0.12, [Redshift](http://jonls.dk/redshift/) is supported, via a
[patchset](https://github.com/Lourens-Rich/redshift.git).
These patches are available on the Arch Linux User Repository, package `redshift-wayland-git`.
That was mostly the final missing thing for me in order to switch from i3 to Sway.
The only annoyance left is the copy paste from Wayland native applications to XWayland applications,
and vice versa.

Since a while, I started using [Rofi](https://davedavenport.github.io/rofi/)
as my application launcher, because it supports (Freedesktop) Desktop file launchers.

Long story short: Rofi on Sway starts and shows up, but doesn't always respond to keypresses.
Luckily, [Sardem FF7](http://www.sardemff7.net) is working on a
[`wip/wayland` branch](https://github.com/DaveDavenport/Rofi/tree/wip/wayland).

It does use some non-official Wayland protocols, and needs some ~~hacks~~ tweaking here and there.
For convenience, I created two AUR packages; both for
[`wlc-wall-injector`, the hack](https://aur.archlinux.org/packages/wlc-wall-injector-git) and
[`wayland-wall`, the protocols](https://aur.archlinux.org/packages/wayland-wall-git).

Installing the former will also pull in the latter as a dependency.
After that, you'll need to `LD_PRELOAD` the hack.
To start Sway, you'll need to use
`LD_PRELOAD=/usr/lib/libwlc-wall-injector.so sway` from now on.

Now, you can install the `wayland` branch of Rofi.
I made another package for that:
[`rofi-wayland-git`](https://aur.archlinux.org/packages/rofi-wayland-git).

EDIT: you also have to disable the `cap_sys_ptrace=eip` on `/usr/bin/sway`:

```
setcap -r /usr/bin/sway
```

because that otherwise disables your `LD_PRELOAD` hack.

Good luck. Join us on Freenode if you have feedback;
I'm both on `#sway` and `#rofi`.
