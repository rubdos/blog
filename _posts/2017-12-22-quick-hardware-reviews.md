---
layout: post
title: Quick hardware reviews
date: 2017-12-22
type: post
published: true
status: publish
tags: ['hardware']
author: rubdos
---

I am going to go quickly over a few components and their Linux compatibility.

## TV Tuners

I went over a couple of TV tuners before settling down.
Here's my breakdown on the DVB-S2 tuners I tried.

### TeVii S482 dual DVB-S2 tuner

Works very well when you got a single one,
but putting two of them on a motherboard makes the system unstable.

At least, that's what I felt.
If you want a dual tuner, [get it at my shop](/shop)

Driver is mainlined in Linux, firmware is proprietary but easily found.

### TBS tuners

Support wants SSH access in the production machine.
I noped out.

Their driver must be recompiled by hand every kernel update,
and compilation requires a huge part of the kernel source available.

### Digital Devices tuners

Their driver is mainline for older cards,
and their open source depmod driver is easily installed.
The driver even survived going from Fedora 26 to Fedora 27.

I suppose they'll go mainline some day too with their newer cards.

Those things are modular; buy a dual tuner, extend to octa tuner on the same PCIe slot.
That's really awesome.
Reconfiguring/extending the thing doesn't require any driver reconfiguration,
it just gets recognised at boot, and works.

If you call those guys to question Linux support,
you actually get someone technically qualified on the phone without *any* hassle whatsoever.

Terrific experience, this is my default now.

## SATA controllers and AMD motherboards

Situation: Ryzen 1400 on an ASUS A320M-K.
Apparently, this motherboard has a borked iommu implementation,
so the Marvell-based controller I tried didn't get its drives recognised.

Booting with `iommu=soft` "solves" the issue, but it's not pretty.

## Hard drives in a Fractal Design Node 804 case

The Fractal Design 804 case can host 8 disks in bays, and two mounted on the bottom.
It's pretty stuffed when it's full, but it's doable.

Now, take a look at this picture:

![Hard drive screw positions](http://www.computer-answers.ca/wp-content/uploads/2017/06/Seagate_Older_pattern.jpg)

This drive has three screwholes on the side, and four on its back.
For the Node 804 bays, you use the top two holes at the side
(with the bottom having the SATA connectors).
If you mount the disk on the bottom plate, you use the four holes on the back.

Now look at this Seagate Archive 8TB (same as IronWolf 8TB):

![Seagate 8TB screw holes](http://www.storagereview.com/images/StorageReview-Seagate-Archive-8TB-HDD-Bottom.jpg)

This one misses two holes in the middle,
which leaves the drive with a rotational degree of freedom in the Node 804 bays.

The position of the bottom plate holes is also different.
Screwing them to the bottom plate is stable, however.

Just a heads up if you want to go with IronWolfs in a 804 based NAS.
