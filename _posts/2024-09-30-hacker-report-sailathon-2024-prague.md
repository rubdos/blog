---
layout: post
title: 'Hacker report: Sailathon 2024, Prague'
date: 2024-09-30 11:06 +0200
---

Today is the final day of the Sailathon, the Sailfish OS hackathon in Prague.
At the peak this weekend, we were about 20 hackers from all around Europe,
working on various Sailfish OS related projects.
By today, Monday, most of the hackers have already left; the five remaining are still hacking strong!

# Jolla C2 discussions

The star of the show was probably the Jolla C2, a new device that Jolla has been working on.
Sailors Raine and Matti brought a prototype with them.
[Jozef blogged about it and published some pictures](https://blog.mlich.cz/2024/09/sailathon-2024-sailfish-os-hackathon/).
As expected, I was a bit sad with the screen on the device, but it seemed like a great device in any other regard.
Raine was interested in hearing our opinions on the home screen rotation feature, and we had a good discussion about it over a beer or two (or three or four, I lost count).
Looking forward to playing with it more when I get mine the next weeks!

# Taak, and Rust apps in OBS

[Taak](https://gitlab.com/rubdos/taak/) is a simple frontend for TaskWarrior, written in Rust.
I wrote it with two intentions: to make the simplest, cleanest Rust-app for Sailfish OS that I could think of,
and to have a simple frontend for TaskWarrior on my phone.
It also serves as a test ground [for my Rust apps](https://gitlab.com/whisperfish/sailo-rs).

[As @poetaster wrote on the forum](https://forum.sailfishos.org/t/september-27-30-sailathon-24-in-prague/19912/125?u=rubdos):
> I wanted to add that those two [@xmlich02 and @rubdos] propelled @pherjung to get a nearly complete rust env running and integrate with obs. It’s now up to us to help @pherjung complete the process and document how you set up rust dependencies (vendor dir ahoi!) to compile on obs … or @nephros will simply provide a cheatsheet and we’re all set 🙂

@pherjung and @xmlich02 were basically working on getting Rust apps -- rubdos-style -- to build in OBS, and as such Chum.
If they succeed to build Taak in OBS, Whisperfish will soon follow!

# Progress on Whisperfish

Matti and I have been working on [Whisperfish](https://gitlab.com/whisperfish/whisperfish/), a Signal client for Sailfish OS.
My personal goal for the weekend was to get voice calls working.
I'm still compiling and testing as I'm writing this post, but I'm confident that we'll have something to show soon!

Matti guided [hoxifi](https://forum.sailfishos.org/u/hoxifi/summary) to his [first Whisperfish patches](https://gitlab.com/whisperfish/whisperfish/-/merge_requests?scope=all&state=all&author_username=hoxifi).
The [first patch](https://gitlab.com/whisperfish/whisperfish/-/merge_requests/632) is a much needed QtCreator project file,
to get new contributors started with Whisperfish development in the more familiar [Sailfish IDE](https://docs.sailfishos.org/Develop/Apps/Your_First_App/).

Meanwhile, Matti and I made quite some progress.
Linked devices was broken (again, again),
because of some missing exchange of key material.
Matti [implemented the remaining bits](https://gitlab.com/whisperfish/whisperfish/-/merge_requests/610) to fix that issue.

With some help of poetaster, we got [voice note volume normalization](https://gitlab.com/whisperfish/whisperfish/-/merge_requests/615) mostly done.
Turns out it's difficult to test, because the Xperia 10 IV has a [broken audio recorder](https://forum.sailfishos.org/t/feedback-on-xperia-10-iv/20113/15?u=rubdos) at the moment.

The bulk of my own energy went into [voice call support](https://gitlab.com/whisperfish/whisperfish/-/merge_requests/595/) (and soon™ video call support).
As [reported earlier](2024-09-08-building-ringrtc-for-whisperfish.md), I had managed to get RingRTC and WebRTC to link in Whisperfish.
The remainder is "easy": link up the RingRTC API to the Whisperfish storage, networking, GUI, and multimedia processing frameworks.
The past few days were spent integrating the ringing behaviour.
Raine Mäkeläinen helped a lot on Saturday: he provided me with a demo application for the necessary MCE and ring tone integrations,
which I could easily copy-paste into the Whisperfish QML code.
I'd also like to thank Sebix and Pherjung to call me several times during the day, so I could test the ringing behaviour.
At the time of writing (nephros just went home, three of us [left in the Spálený Kafe coffee shop](https://europeancoffeetrip.com/cafe/spalenykafe-prague/)),
I'm figuring out the [networking](https://github.com/whisperfish/libsignal-service-rs/pull/327) and multimedia.

![Voice call UI element](/assets/img/matti-voice-call.png)

# Closing note

I witnessed a lot of progress around me on several other apps (Tooter, Amazfish come to mind).
I hope the other community members will blog about their progress too;
this post would become way too long to summarize all the nice things that have been done.

Hereafter are some pictures to set the stage.
It was a rather pleasant experience meeting all those people,
and I hope to see this group again on the next Sailathon.

![Me in his natural habitat: a coffee shop](/assets/2024-prague/DSC01930.JPG)
![Xperia's on display in a Sony shop](/assets/2024-prague/DSC01986.JPG)
![Early arrivals sharing dinner and beer](/assets/2024-prague/DSC01989.JPG)
![Obligatory Tux selfie](/assets/2024-prague/DSC02001.JPG)
![Half the group for lunch](/assets/2024-prague/DSC02004.JPG)
![A closeby church by night](/assets/2024-prague/DSC02074.JPG)
