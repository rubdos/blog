---
layout: page
title: Chess
subtitle: My chess games
permalink: /chess/
---

I play chess at the [Schaakkring Oude God Mortsel](http://www.skoudegod.be/).
At time of writing, I have around 1200 [ELO](http://www.frbe-kbsb.be/sites/manager/GestionFICHES/FRBE_Fiche.php?matricule=12187)'s.

Here are my past interclub games.
As I [annotate](http://pychess.org/) them, they go straight into a [git repository](https://gitlab.com/rubdos/interclub),
and [a rebuild of my website is triggered](https://gitlab.com/rubdos/blog/pipelines) when GitLab receives the new PGN file.
This way, my games are usually online the evening of the game.

When I have some time, I will try to get the comments/annotations that I make/are made by [Stockfish](https://stockfishchess.org/)
visible on the page of the game.

<hr />

{% assign games = site.chess_games | sort: 'date' | reverse %}
{% for game in games %}
  [**{{ game.date | date: '%d/%m/%Y' }}** *{{ game.game_subtitle }}* **{{ game.game.white }}** vs. **{{ game.game.black }}** ({{ game.result }})]({{ game.url | relative_url }})
{% endfor %}

<hr />

*Visualization of the chessboards using [chessboard.js by Chris Oakman](http://chessboardjs.com/)*
