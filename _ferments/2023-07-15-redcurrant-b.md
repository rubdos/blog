---
layout: page
date: 2023-07-15
title: Red currant mead
type: mead
redirect_from:
- /c97b205b-4512/
---

I got my hands on 6kg of redcurrant, which I cleaned and froze shortly after harvest.
I will make six batches of five litres in total,
with slight A/B variations: I make 10 litres at a time, in two separate carboys with a slight variation between them.

This is the first recipe, and the B-variant will have a different honey.
The other parameters will be remain fixed.

I will start with 1.6kg of honey and 1kg of currant (which should have a 1.100 theoretical SG),
and will add 250g of blueberries and 0.4kg of honey in secondary to get the sweetness up,
and introduce some fruity complexity.

* Honey: 1.636 kg Delhaize "bloemenhoning", vast
* Red currant: 1072g
* Yeast: QA23, 2.5g
* Nutrivit: 2.5g
* Yeast hulls: 2.5g
* Bentonite: 2.8g
* Pectic enzyme: 5.3g
* H2O: 3.163kg: added as much as to get to approx 5L

All of the above in primary.

Log:

# July 2 2023: foraging

Harvested 6kg of red currant.
Cleaned and frozen over the course of three days.

# July 15 2023: pitching

Threw everything in the 5L buckets, rehydrated yeast at 38°C, and pitched it in the way too cold, barely mixed must.
QA23 is a beast, it started bubbling right away.

Est. SG: 1.1

# July 30 2023: yeast settled

Pulled out the fruit, measured SG 0.94.
Added 250g of blue berries and 320g of honey.

# August 27 2023: fermentation halted again

(This one took slightly longer than the A-version)

Racked onto 0.6g of potassium metabisulphite

Measured 1.010, so 15% ABV
