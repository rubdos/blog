---
layout: page
date: 2022-12-11
type: hot sauce
title: Persimmon Habanero hot sauce
---

* Carbonero: 27g
* Habanero: 123g

(total peppers with stalks: 150g)

Persimmon: 768g

Salt: 2.5% = 23g

![Start of fermentation](/assets/img/20221211_sauce.jpg)
