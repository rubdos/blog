---
layout: page
date: 2023-03-16
title: Viking Midget blood short mead, second attempt
type: mead
redirect_from:
- /0131abc0-1e4d/
---

Loosely based on [Reddit recipe](https://www.reddit.com/r/mead/wiki/userrecipes/melomel/#wiki_the_viking_midget).
Previous attempt at similar recipe [resulted in vinegar](/private/ferments/2023-01-23-viking-midget.html), presumably because of a particularly airy racking after primary.

* Honey: 750g Famille Michaud - Lune de Miel, €9.99/kg 1+1 gratis.
* Cherries: 920g (Colruyt, Boni "Seizoensfruit", frozen)
* Yeast: QA23: 2.3g
* Yeast hulls: 3g
* Pectinase: 2.9g
* Bentonite: 9.2g
* H2O: added as much as to get to approx 5L

All of the above in primary.
Did not boil fruit first,
but defrosted it by pouring hot water over it together with the honey until room temp.

Log:

# March 16 2023

![Initial fermentation](/assets/img/20230123-viking-midget.jpg)

Starting gravity measured @ 1.050 (but fruit...)
Calculated gravity 1.053
Potential 7.3%

Yeast did not seem to rehydrate very well. No bubbles tomorrow and will repitch.

# March 26, 2023

Fermentation seems to have halted.  SG 0.98.  Testes a bit hollow, even when sweetened, but maybe that's the spiciness of the higher alcohols.

Racked off the fruit onto 1g of sorbate and 2g of sulphites (yes I know, I misweighed, I need a better scale).

# April 23, 2023

Racked onto 257g of "Famille Michaud - Lune de Miel" honey.

# May 28, 2023

Bottled into 11 "Ritchie" bottles and one wine bottle.
