---
layout: page
date: 2023-01-23
title: Viking Midget blood short mead
type: mead
---

Based on [Reddit recipe](https://www.reddit.com/r/mead/wiki/userrecipes/melomel/#wiki_the_viking_midget)

* Honey: 500g Colruyt "honing" (100% bloemenhoning).
* Cherries: 1kg (Colruyt, frozen)
* Yeast: QA23: 2.5g
* VitaFerm Ultra F3: 3g
* Pectinase: 2.9g
* Bentonite: 3.5g
* H2O: added as much as to get to approx 4L

All of the above in primary.

Log:

# January 23 2023

![Initial fermentation](/assets/img/20230123-viking-midget.jpg)

Starting gravity measured @ 1.050 (but fruit...)
Calculated gravity 1.054
Potential 7.4% (but total volume is not exactly sure)

# January 26 2023

Fermentation halted, SG measured 1.000, so ABV 6.5% - 7.1%, depending on fermented fruit sugar content.

Slight sulfuric smell.

# February 16 2023

Most fruit has set to the bottom, time to rack and stabilize.
I plan a back sweetening round on February 18.

SG 0.990 ± 0.01 (ABV 6.5% - 9.1%, including uncertainty on the cherry sugar. Most probably around 7.7%)

Racked with:

- 8 grams of Isinglass (should've been 4g, but forgot to shake the bottle)
- 0.4g of potassium sorbate
- 0.4g of potassium metabisulphite

# February 24 2023

Backsweetened with approx. 110g of Cuban coffee honey and 70g of acacia honey.

Volume est 3100mL

# March 12 2023

A week ago, started having signs of an infection.
Dumped. Tasted like sweet cherry vinegar. 😢
