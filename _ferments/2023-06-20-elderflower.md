---
layout: page
date: 2023-06-20
title: Elderflower mead
type: mead
redirect_from:
- /d64c5416-c8b0/
---

Based on [Modern Meadmaking beginner metheglyn](https://meadmaking.wiki/en/recipes/beginner/0002),
but swapping parts honey out for half a liter of elderflower syrup that I was gifted.

The properties of the syrup are unknown (but it tastes very nice and strongly of elderflower),
so I measure the sugar content first.

Target starting SG: 1.110

* Elderflower syrup: 386.4g @ 340mL (SG 1.136) before pasteurisation, so 117 grams of sugar
* Honey: 1.651 kg Delhaize "bloemenhoning", vast
* Yeast: Brewferm "Super Saison" beer yeast: 3.3g
* Nutrivit: 4.2g
* Yeast hulls: 2.7g
* Bentonite: 4.0g
* H2O: 3.5kg: added as much as to get to approx 5L

All of the above in primary.

Log:

# June 20 2023: pitching

Pasteurized the elderflower syrup at 78°C, which evaporated about 20 grams of water.

Ended up at SG 1.112, which is somewhere at the 14% ABV range. Given I'm using a beer yeast, I'm expecting to stop early.

# July 15: racking and stabilizing

FG 1.032 (10.7% ABV), not sure whether 100% stable already.

# September 09: bottled

Forgot to measure again, but did not spot any activity. Assuming 11% ABV, just as intended :-)

Bottled on 11 "ritchie" bottles and one corked 75cl for longer aging and storage.
