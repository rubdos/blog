---
layout: page
title: Fermentations
subtitle: My fermentation log
permalink: /private/ferments/
unlist: true
---

# History

{% assign ferments = site.ferments | sort: 'date' | reverse %}
<ul style="list-style: none;">
{% for ferment in ferments %}
  <li><a href="{{ ferment.url }}">{{ ferment.date | date: '%B %d %Y' }}: {{ ferment.title }}</a></li>
{% endfor %}
</ul>

# Mead wishlist

- Sweet cherry
- Raspberry
- Blueberries
