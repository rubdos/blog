ifeq (${CI_PROJECT_DIR},)
CI_PROJECT_DIR:=.
endif

SITE_DIR=/tmp/_site/

.PHONY: build

build: assets/cv.pdf chess_archive.tar assets/js/chessboard-0.3.0.min.js assets/sheetmusic
	mkdir -p ${SITE_DIR}
	bundle exec jekyll build -s ${CI_PROJECT_DIR} -d ${SITE_DIR}

## CV

assets/cv.pdf:
	wget https://gitlab.com/rubdos/cv/-/jobs/artifacts/master/download?job=build -O /tmp/cv.zip
	unzip /tmp/cv.zip -d /tmp/
	cp /tmp/cv.pdf assets/cv.pdf -f
	cp /tmp/cv.nl.pdf assets/cv.nl.pdf -f

## CHESS

chess_archive.tar:
	wget -O chess_archive.tar https://gitlab.com/rubdos/interclub/-/archive/master/interclub-master.tar

chessboardjs-0.3.0.zip:
	wget -O chessboardjs-0.3.0.zip http://chessboardjs.com/releases/0.3.0/chessboardjs-0.3.0.zip

assets/js/chessboard-0.3.0.min.js: chessboardjs-0.3.0.zip
	unzip chessboardjs-0.3.0.zip -d assets
	touch assets/js/chessboard-0.3.0.min.js

## MUSIC

music.zip:
	rm -rf assets/sheetmusic
	wget -O music.zip https://gitlab.com/rubdos/music/-/jobs/artifacts/master/download?job=build

assets/sheetmusic: music.zip
	mkdir -p assets/sheetmusic
	unzip -d assets/sheetmusic music.zip

deploy:
	ls
	pwd
	rsync --progress --recursive --verbose ${SITE_DIR} rubdos.be@rubdos.be:/var/www/rubdos.be/
