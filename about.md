---
layout: page
title: About
subtitle: Ruben De Smet
permalink: /about/
---

*Hi*

I'm Ruben De Smet, I'm about (`current_year`-1995) years old,
and do stuff with computers.

Because I (usually) like computers, I am currently working for [my PhD in computer science](https://www.etrovub.be/people/member/about-bio/rdesmeta/).
Sometimes, people contact me for some weekend work.
Have a look at my [curriculum vitae](/assets/cv.pdf) ([ook beschikbaar in het Nederlands](/assets/cv.nl.pdf)), if that might be of interest!

I also like [Rust](http://www.rust-lang.org), GNU, [free software](https://www.gnu.org/philosophy/free-sw.html), and Linux.
My favorite operating systems are Arch Linux, Fedora and SailfishOS.

I have accounts on several online platforms; you can find links in the bottom of this page.
[I am not a used of Facebook](https://www.fsf.org/facebook).
You can also contact me by [Signal on @rubdos.95](https://signal.me/#eu/snJWgAFbLBsBhexpsKuL2SpDuTgohfrfZqvz7C3oalAnZv2pAPdFxar-zJC-0bNE) and e-mail (me [at] rubdos [dot] be).
Just make sure to substitute [dot] and [at] with “.” and “@”.

<a href="https://signal.me/#eu/snJWgAFbLBsBhexpsKuL2SpDuTgohfrfZqvz7C3oalAnZv2pAPdFxar-zJC-0bNE"><img width="50%" src="{{ 'assets/img/signal-username-qr-code.png' | relative_url }}" alt="rubdos.95 on Signal"></a>

If you ever need to share a file with me, you can use

<a target="_blank" rel="noreferrer noopener" href="https://nextcloud.com/sharing#rsmet@nextcloud.rubdos.be" style="padding:10px;background-color:#0082c9;color:#ffffff;border-radius:3px;padding-left:4px;">
<span style="background-image:url(https://nextcloud.rubdos.be/core/img/logo/logo.svg?v=0);width:50px;height:30px;position:relative;top:8px;background-size:contain;display:inline-block;background-repeat:no-repeat; background-position: center center;"></span>
 my Nextcloud</a>
